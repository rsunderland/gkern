package org.rimasu.gkern.op;

import org.junit.Before;
import org.junit.Test;
import org.rimasu.gkern.grid.api.ArrayBook;
import org.rimasu.gkern.grid.api.Book;
import org.rimasu.gkern.grid.api.RowCursor;
import org.rimasu.gkern.grid.api.Table;
import org.rimasu.gkern.grid.array.IntArrayTable;
import org.rimasu.gkern.grid.buf.BufferFailure;
import org.rimasu.gkern.op.api.Result;

import java.nio.ByteBuffer;
import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.rimasu.gkern.grid.api.BookMatcher.matchesBook;

public class PackBookOperatorTest {

    private PackBookOperator op;

    @Before
    public void createOperator() {
        op = new PackBookOperator();
    }


    @Test
    public void canRoundTripEmptyBook() {
        Book in = book();
        ByteBuffer buffer = op.pack(in);

        assertThat(buffer.remaining(),is(7));

        Result<Book, BufferFailure> result = op.unpack(buffer);

        assertTrue(result.isOk());
        assertThat(result.get(),matchesBook(in));
    }

    @Test
    public void canRoundTripBook() {
        Book in = book(
                table(2,
                        row(1, 3),
                        row(4, 100)
                ),
                table( 3,
                        row(0, Integer.MIN_VALUE+1, Integer.MAX_VALUE)
                )
        );

        ByteBuffer buffer = op.pack(in);


        assertThat(buffer.remaining(),is(35));



        Result<Book, BufferFailure> result = op.unpack(buffer);

        assertTrue(result.isOk());
        assertThat(result.get(),matchesBook(in));
    }


    private Table table(int numColumns, Integer[]... rows) {
        IntArrayTable table = new IntArrayTable(numColumns, rows.length);
        RowCursor cursor = table.createCursor();
        for(Integer[] row : rows) {
            assertThat(row.length,is(numColumns));
            assertTrue(cursor.moveToNextRow());

            int i=1;
            for(Integer cell : row) {
                if(cell == null) {
                    cursor.clearValue(i);
                } else {
                    cursor.setValue(i, cell);
                }
                i++;
            }

        }
        return table;
    }

    private Integer[] row(Integer... values) {
        return values;
    }

    private Book book(Table... tables) {
        return new ArrayBook(Arrays.asList(tables));
    }
}
