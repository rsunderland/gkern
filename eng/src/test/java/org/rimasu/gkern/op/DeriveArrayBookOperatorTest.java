package org.rimasu.gkern.op;


import org.junit.Before;
import org.junit.Test;
import org.rimasu.gkern.grid.api.*;
import org.rimasu.gkern.grid.array.IntArrayTable;
import org.rimasu.gkern.grid.array.IntArrayTableFactory;
import org.rimasu.gkern.op.api.Result;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.rimasu.gkern.op.MoveFailureType.*;

public class DeriveArrayBookOperatorTest {


    private DeriveBookOperator operator;
    private int numOriginalTables = 2;
    private int numAddedTables = 2;
    private int numRemovedTables = 2;
    private Integer[][] originalRows;
    private Integer[][] addedRows;
    private Integer[][] removedRows;
    private Result<Book, MoveFailure> result;

    @Before
    public void createOperator() {
        operator = new DeriveBookOperator(new IntArrayTableFactory());
    }

    @Test
    public void tableWithSameValuesAsOriginalTableReturnedIfAddedAndRemovedAreEmpty() {
        givenOriginalTableOneHasRows(5, 10, 12);
        givenOriginalTableTwoHasRows(2, 3);

        whenOperatorWasApplied();

        resultWasOk();

        tableOneHadRows(5, 10, 12);
        tableTwoHadRows(2, 3);
    }

    @Test
    public void removedRowsAreRemovedFromTable() {

        givenOriginalTableOneHasRows(2, 4, 5, 6, 10);
        givenRemovedTableOneHasRows(4, 6);

        givenOriginalTableTwoHasRows(1, 3);
        givenRemovedTableTwoHasRows(1);

        whenOperatorWasApplied();

        resultWasOk();

        tableOneHadRows(2, 5, 10);
        tableTwoHadRows(3);
    }

    @Test
    public void failureReturnedIfRemovedRowsNotFoundInOriginalTable() {
        givenOriginalTableOneHasRows(2, 4, 5, 6, 10);
        givenRemovedTableOneHasRows(5, 7);

        whenOperatorWasApplied();

        resultFailedWith(REMOVED_ROW_NOT_FOUND_IN_ORIGINAL_TABLE);
    }

    @Test
    public void failureReturnedIfAddedRowsNotFoundInOriginalTableAndNotAlreadyRemoved() {

        givenOriginalTableTwoHasRows(2, 4, 5, 6, 10);
        givenAddedTableTwoHasRows(3, 6);

        whenOperatorWasApplied();

        resultFailedWith(ADDED_ROW_ALREADY_IN_ORIGINAL_TABLE);
        resultFailedOnTable(2);
    }


    @Test
    public void failureReturnedIfAddedRowsAreNotFullyOrdered() {

        givenOriginalTableOneHasRows(2, 4, 6, 8, 10);
        givenAddedTableOneHasRows(1, 3, 5, 5);

        whenOperatorWasApplied();

        resultFailedWith(ADDED_ROWS_NOT_FULLY_ORDERED);
        resultFailedOnTable(1);
    }


    @Test
    public void failureReturnedIfRemovedRowsAreNotFullyOrdered() {

        givenOriginalTableOneHasRows(2, 4, 6, 8, 10);
        givenRemovedTableOneHasRows(2, 8, 6);

        whenOperatorWasApplied();

        resultFailedWith(REMOVED_ROWS_NOT_FULLY_ORDERED);
    }


    @Test
    public void addedRowsAreAddedToTable() {

        givenOriginalTableOneHasRows(2, 4, 5);
        givenAddedTableOneHasRows(3, 7);

        givenOriginalTableTwoHasRows(2, 4, 5);
        givenAddedTableTwoHasRows(3);


        whenOperatorWasApplied();

        resultWasOk();
        tableOneHadRows(2, 3, 4, 5, 7);
        tableTwoHadRows(2, 3, 4, 5);
    }



    @Test
    public void failureReturnedIfAddedBookHasTooFewTables() {
        givenNumberOfTablesInAddedBookIs(1);

        whenOperatorWasApplied();

        resultFailedWith(ADDED_BOOK_HAS_TOO_FEW_TABLES);
    }

    @Test
    public void failureReturnedIfAddedBookHasTooManyTable() {
        givenNumberOfTablesInAddedBookIs(3);

        whenOperatorWasApplied();

        resultFailedWith(ADDED_BOOK_HAS_TOO_MANY_TABLES);
    }

    private void givenNumberOfTablesInAddedBookIs(int numTables) {
        numAddedTables = numTables;
    }

    @Test
    public void failureReturnedIfRemovedBookHasTooFewTables() {
        givenNumberOfTablesInRemovedBookIs(1);

        whenOperatorWasApplied();

        resultFailedWith(REMOVED_BOOK_HAS_TOO_FEW_TABLES);
    }

    @Test
    public void failureReturnedIfRemovedBookHasTooManyTable() {
        givenNumberOfTablesInRemovedBookIs(3);

        whenOperatorWasApplied();

        resultFailedWith(REMOVED_BOOK_HAS_TOO_MANY_TABLES);
    }

    private void givenNumberOfTablesInRemovedBookIs(int numTables) {
        numRemovedTables = numTables;
    }


    private void tableOneHadRows(Integer... expectedValues) {
        Book actualBook = result.get();
        Iterator<Table> tables = actualBook.iterator();
        Table actual = tables.next();
        assertTableHasRows(actual, expectedValues);
    }

    private void tableTwoHadRows(Integer... expectedValues) {
        Book actualBook = result.get();
        Iterator<Table> tables = actualBook.iterator();
        tables.next();
        Table actual = tables.next();
        assertTableHasRows(actual, expectedValues);
    }

    private void resultWasOk() {
        assertNotNull(result);
        assertTrue(result.isOk());
    }

    private void assertTableHasRows(Table actual, Integer[] expectedValues) {
        assertThat(actual.getNumRows(), is(expectedValues.length));

        RowCursor c = actual.createCursor();
        for (Integer expectedValue : expectedValues) {
            c.moveToNextRow();
            if (expectedValue == null) {
                assertTrue(c.isClear(1));
            } else {
                assertFalse(c.isClear(1));
                assertThat(c.getValue(1), is(expectedValue));
            }
        }
        assertFalse(c.moveToNextRow());
    }

    private void resultFailedWith(MoveFailureType failure) {
        assertNotNull(result);
        assertTrue(result.isFailure());
        assertThat(result.getFailureDetails().getTableFailure(), is(failure));
    }

    private void resultFailedOnTable(int tableNum) {
        assertNotNull(result);
        assertTrue(result.isFailure());
        assertTrue(result.getFailureDetails().getTableNum().isPresent());
        assertThat(result.getFailureDetails().getTableNum().get(),is(tableNum));
    }


    private void givenAddedTableOneHasRows(Integer... rows) {
        lazyBuildAddedRows();
        addedRows[0] = rows;
    }

    private void lazyBuildAddedRows() {
        if(addedRows == null) {
            addedRows = new Integer[numAddedTables][];
        }
    }

    private void givenRemovedTableOneHasRows(Integer... rows) {
        lazyBuildRemovedRows();
        removedRows[0] = rows;
    }

    private void lazyBuildRemovedRows() {
        if(removedRows == null) {
            removedRows = new Integer[numRemovedTables][];
        }
    }

    private void givenOriginalTableOneHasRows(Integer... rows) {
        lazyBuildOriginalRows();
        originalRows[0] = rows;
    }

    private void lazyBuildOriginalRows() {
        if(originalRows == null) {
            originalRows = new Integer[numOriginalTables][];
        }
    }

    private void givenAddedTableTwoHasRows(Integer... rows) {
        lazyBuildAddedRows();
        addedRows[1] = rows;
    }

    private void givenRemovedTableTwoHasRows(Integer... rows) {
        lazyBuildRemovedRows();
        removedRows[1] = rows;
    }

    private void givenOriginalTableTwoHasRows(Integer... rows) {
        lazyBuildOriginalRows();
        originalRows[1] = rows;
    }


    private void whenOperatorWasApplied() {
        lazyBuildRemovedRows();
        lazyBuildAddedRows();
        lazyBuildOriginalRows();

        ArrayBook original = buildBook(originalRows);
        ArrayBook added = buildBook(addedRows);
        ArrayBook removed = buildBook(removedRows);

        result = operator.apply(original, removed, added);
    }

    private ArrayBook buildBook(Integer[][] rows) {
        Collection<Table> tables = new ArrayList<>();
        for(Integer[] tableRows : rows) {
            tables.add(buildTable(tableRows));
        }
        return new ArrayBook(tables);
    }

    private Table buildTable( Integer[] rows) {
        int numRows = rows == null ? 0 : rows.length;
        IntArrayTable table = new IntArrayTable(1, numRows);
        populateRows(rows, table);
        return table;
    }

    private void populateRows(Integer[] rows, IntArrayTable table) {
        if (rows != null) {
            RowCursor c = table.createCursor();
            for (Integer row : rows) {
                c.moveToNextRow();
                if (row == null) {
                    c.clearValue(1);
                } else {
                    c.setValue(1, row);
                }
            }
        }
    }

}
