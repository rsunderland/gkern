package org.rimasu.gkern.op;


import org.junit.Before;
import org.junit.Test;
import org.rimasu.gkern.grid.api.*;
import org.rimasu.gkern.grid.array.IntArrayTable;
import org.rimasu.gkern.grid.array.IntArrayTableFactory;
import org.rimasu.gkern.op.api.Result;


import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.rimasu.gkern.op.MoveFailureType.*;

public class DeriveTableOperatorTest {

    private DeriveTableOperator operator;
    private int numOriginalColumns;
    private int numAddedColumns;
    private int numRemovedColumns;

    private Integer[] originalRows;
    private Integer[] addedRows;
    private Integer[] removedRows;
    private Result<Table, MoveFailureType> result;

    @Before
    public void createOperator() {
        operator = new DeriveTableOperator(new IntArrayTableFactory());
        numOriginalColumns = 1;
        numAddedColumns = 1;
        numRemovedColumns = 1;
    }

    @Test
    public void tableWithSameValuesAsOriginalTableReturnedIfAddedAndRemovedAreEmpty() {
        givenOriginalTableHasRows(5, 10, 12);

        whenOperatorWasApplied();

        resultWasOkAndHadRows(5, 10, 12);
    }

    @Test
    public void removedRowsAreRemovedFromTable() {

        givenOriginalTableHasRows(2, 4, 5, 6, 10);
        givenRemovedTableHasRows(4, 6);

        whenOperatorWasApplied();

        resultWasOkAndHadRows(2, 5, 10);
    }

    @Test
    public void failureReturnedIfRemovedRowsNotFoundInOriginalTable() {
        givenOriginalTableHasRows(2, 4, 5, 6, 10);
        givenRemovedTableHasRows(5, 7);

        whenOperatorWasApplied();

        resultFailedWith(REMOVED_ROW_NOT_FOUND_IN_ORIGINAL_TABLE);
    }

    @Test
    public void failureReturnedIfAddedRowsNotFoundInOriginalTableAndNotAlreadyRemoved() {

        givenOriginalTableHasRows(2, 4, 5, 6, 10);
        givenAddedTableHasRows(3, 6);

        whenOperatorWasApplied();

        resultFailedWith(ADDED_ROW_ALREADY_IN_ORIGINAL_TABLE);
    }



    @Test
    public void failureReturnedIfAddedRowsAreNotFullyOrdered() {

        givenOriginalTableHasRows(2, 4, 6, 8, 10);
        givenAddedTableHasRows(1, 3, 5, 5);

        whenOperatorWasApplied();

        resultFailedWith(ADDED_ROWS_NOT_FULLY_ORDERED);
    }


    @Test
    public void failureReturnedIfRemovedRowsAreNotFullyOrdered() {

        givenOriginalTableHasRows(2, 4, 6, 8, 10);
        givenRemovedTableHasRows(2, 8, 6);

        whenOperatorWasApplied();

        resultFailedWith(REMOVED_ROWS_NOT_FULLY_ORDERED);
    }

    @Test
    public void failureReturnedIfAddedTableHasTooFewRows() {
        givenNumberOfColumnsInAddedTableIs(0);

        whenOperatorWasApplied();

        resultFailedWith(ADDED_TABLE_HAD_TOO_FEW_COLUMNS);
    }

    @Test
    public void failureReturnedIfAddedTableHasTooManyRows() {
        givenNumberOfColumnsInAddedTableIs(2);

        whenOperatorWasApplied();

        resultFailedWith(ADDED_TABLE_HAD_TOO_MANY_COLUMNS);
    }

    @Test
    public void failureReturnedIfRemovedTableHasTooFewRows() {
        givenNumberOfColumnsInRemovedTableIs(0);

        whenOperatorWasApplied();

        resultFailedWith(REMOVED_TABLE_HAD_TOO_FEW_COLUMNS);
    }

    @Test
    public void failureReturnedIfRemovedTableHasTooManyRows() {
        givenNumberOfColumnsInRemovedTableIs(2);

        whenOperatorWasApplied();

        resultFailedWith(REMOVED_TABLE_HAD_TOO_MANY_COLUMNS);
    }

    private void givenNumberOfColumnsInAddedTableIs(int numColumns) {
        numAddedColumns = numColumns;
    }

    private void givenNumberOfColumnsInRemovedTableIs(int numColumns) {
        numRemovedColumns = numColumns;
    }


    @Test
    public void addedRowsAreAddedToTable() {

        givenOriginalTableHasRows(2, 4, 5);
        givenAddedTableHasRows(3, 7);

        whenOperatorWasApplied();

        resultWasOkAndHadRows(2, 3, 4, 5, 7);
    }


    private void resultWasOkAndHadRows(Integer... expectedValues) {
        assertNotNull(result);
        assertTrue(result.isOk());
        Table actual = result.get();
        assertThat(actual.getNumRows(), is(expectedValues.length));

        RowCursor c = actual.createCursor();
        for (Integer expectedValue : expectedValues) {
            c.moveToNextRow();
            if (expectedValue == null) {
                assertTrue(c.isClear(1));
            } else {
                assertFalse(c.isClear(1));
                assertThat(c.getValue(1), is(expectedValue));
            }
        }
        assertFalse(c.moveToNextRow());
    }

    private void resultFailedWith(MoveFailureType failure) {
        assertNotNull(result);
        assertTrue(result.isFailure());
        assertThat(result.getFailureDetails(), is(failure));
    }


    private void givenAddedTableHasRows(Integer... rows) {
        addedRows = rows;
    }

    private void givenRemovedTableHasRows(Integer... rows) {
        removedRows = rows;
    }

    private void givenOriginalTableHasRows(Integer... rows) {
        originalRows = rows;
    }


    private void whenOperatorWasApplied() {
        Table original = buildTable(numOriginalColumns, originalRows);
        Table added = buildTable(numAddedColumns, addedRows);
        Table removed = buildTable(numRemovedColumns, removedRows);

        result = operator.apply(original, removed, added);
    }


    private Table buildTable(int numColumns, Integer[] rows) {
        int numRows = rows == null ? 0 : rows.length;
        IntArrayTable table = new IntArrayTable(numColumns, numRows);
        populateRows(rows, table);
        return table;
    }

    private void populateRows(Integer[] rows, IntArrayTable table) {
        if (rows != null) {
            RowCursor c = table.createCursor();
            for (Integer row : rows) {
                c.moveToNextRow();
                if (row == null) {
                    c.clearValue(1);
                } else {
                    c.setValue(1, row);
                }
            }
        }
    }

}
