package org.rimasu.gkern.op.api;

import org.junit.Test;
import org.rimasu.gkern.grid.api.KernelErrorCode;
import org.rimasu.gkern.grid.api.KernelException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ResultTest {

    private static final String VALUE = "some value";
    private static final Integer FAILURE_DETAILS = 15;

    @Test
    public void okResultHasValue() {
        assertThat(okResult().get(), is(VALUE));
    }

    @Test
    public void okResultReturnTrueForIsOk() {
        assertTrue(okResult().isOk());
    }

    @Test
    public void okResultReturnsFalseForIsFailure() {
        assertFalse(okResult().isFailure());
    }

    @Test
    public void okResultHasNoFailureDetails() {
        try {
            okResult().getFailureDetails();
        } catch (KernelException e) {
            assertThat(e.getCode(),is(KernelErrorCode.OK_RESULT_HAS_NO_FAILURE_DETAILS));
        }
    }

    @Test
    public void failureResultHasNoValue() {
        try {
            failureResult().getFailureDetails();
        } catch (KernelException e) {
            assertThat(e.getCode(),is(KernelErrorCode.FAILURE_RESULT_HAS_NO_VALUE));
        }
        assertThat(okResult().get(), is(VALUE));
    }

    @Test
    public void failureResultReturnFalseForIsOk() {
        assertFalse(failureResult().isOk());
    }

    @Test
    public void failureResultReturnsTrueForIsFailure() {
        assertTrue(failureResult().isFailure());
    }

    @Test
    public void failureResultHasFailureDetails() {
        assertThat(failureResult().getFailureDetails(),is(FAILURE_DETAILS));
    }

    private Result<String, Integer> okResult() {
        return Result.ok(VALUE, Integer.class);
    }

    private Result<String, Integer> failureResult() {
        return Result.failure(String.class, FAILURE_DETAILS);
    }
}
