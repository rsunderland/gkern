package org.rimasu.gkern.grid.cursors;


import org.junit.Before;
import org.junit.Test;
import org.rimasu.gkern.grid.api.*;
import org.rimasu.gkern.grid.array.IntArrayTable;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.rimasu.gkern.grid.api.RowViewMatcher.hasValues;

public class RangeCheckingCursorTest {

    private static final int MAX_ROWS = 2;
    private static final int NUM_ROWS = 2;
    private TableShape shape;
    private Table table;
    private RowCursor delegate;
    private RangeCheckingCursor cursor;

    @Before
    public void createTableAndCursor() {
        shape = tableShape(MAX_ROWS, columnShape(1, 10), columnShape(10, 20));
        table = createTable(2, NUM_ROWS);
        delegate = table.createCursor();
        cursor = new RangeCheckingCursor(shape, delegate);
    }

    Table createTable(int numColumns, int numRows) {
        return new IntArrayTable(numColumns, numRows);
    }

    @Test
    public void canSetAndGetValues() {
        setRowValues(1, 1, 11);
        setRowValues(2, 3, 12);
        assertTrue(cursor.moveToNextRow());
        assertThat(cursor, hasValues(1,11));
        assertTrue(cursor.moveToNextRow());
        assertThat(cursor, hasValues(3,12));
        assertFalse(cursor.moveToNextRow());
    }

    @Test
    public void canDetectIfValuesAreInRange() {
        setRowValues(1, 1, 10);
        setRowValues(2, 3, 11);

        scanTable();

        assertTrue(cursor.isInRange());
    }

    @Test
    public void canDetectIfSecondValueIsOutOfRange() {
        setRowValues(1, 1, 10);
        setRowValues(2, 1, 21);

        scanTable();

        assertFalse(cursor.isInRange());
    }

    @Test
    public void canDetectIfFirstValuesIsOutOfRange() {
        setRowValues(1, 5, 10);
        setRowValues(2, 11, 11);

        scanTable();

        assertFalse(cursor.isInRange());
    }

    @SuppressWarnings("PMD")
    private void scanTable() {
        while(cursor.moveToNextRow()) {
            // Nothing to do
        }
    }

    private void setRowValues(int rowNumber, Integer... values) {

        RowCursor row = row(rowNumber);
        int position = 1;
        for(Integer value : values) {
            if(value == null) {
                row.clearValue(position);
            } else {
                row.setValue(position, value);
            }
            position++;
        }
    }

    private RowCursor row(int rowNumber) {
        return table.createCursor().moveToRow(rowNumber);
    }

    private static TableShape tableShape(int maxRows, ColumnShape... columnRanges) {
        return new TableShape(maxRows, Arrays.asList(columnRanges));
    }

    private ColumnShape columnShape(int minInclusive, int maxInclusive) {
        return new ColumnShape(new Range(minInclusive, maxInclusive));
    }
}
