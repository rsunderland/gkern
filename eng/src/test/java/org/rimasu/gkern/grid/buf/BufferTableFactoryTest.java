package org.rimasu.gkern.grid.buf;

import org.junit.Before;
import org.junit.Test;
import org.rimasu.gkern.grid.api.*;
import org.rimasu.gkern.op.api.Result;

import java.nio.ByteBuffer;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.rimasu.gkern.grid.api.KernelErrorCode.NO_BUFFER_FACTORY_FOUND_FOR_VALUE_RANGE;

public class BufferTableFactoryTest {

    private BufferTableFactory factory;

    @Before
    public void createFactory() {
        factory = new BufferTableFactory();
    }

    @Test
    public void canCreateByteTable() {
        Table table = buildTable(4, 5, Byte.MIN_VALUE + 1, Byte.MAX_VALUE);
        assertTrue(table instanceof  ByteBufferTable);
    }

    @Test
    public void canCreateShortTable() {
        Table table = buildTable(4, 5, Short.MIN_VALUE + 1, Short.MAX_VALUE);
        assertTrue(table instanceof ShortBufferTable);
    }

    @Test
    public void canCreateIntTable() {
        Table table = buildTable(4, 5, Integer.MIN_VALUE + 1, Integer.MAX_VALUE);
        assertTrue(table instanceof IntBufferTable);
    }

    @Test
    public void canNotCreateFullIntRangeTable() {
        try {
            buildTable(4,5, Integer.MIN_VALUE, Integer.MAX_VALUE);
            fail();
        } catch( KernelException e) {
            assertThat(e.getCode(),is(NO_BUFFER_FACTORY_FOUND_FOR_VALUE_RANGE));
        }
    }

    @Test
    public void canBindExistingByteTable() {
        Table table = createAndRebindTable(Byte.MIN_VALUE + 1, Byte.MAX_VALUE);
        assertTrue(table instanceof ByteBufferTable);
    }

    @Test
    public void canBindExistingShortTable() {
        Table table = createAndRebindTable(Short.MIN_VALUE + 1, Short.MAX_VALUE);
        assertTrue(table instanceof ShortBufferTable);
    }

    @Test
    public void canBindExistingIntTable() {
        Table table = createAndRebindTable(Integer.MIN_VALUE + 1, Integer.MAX_VALUE);
        assertTrue(table instanceof IntBufferTable);
    }


    private Table createAndRebindTable(int minValue, int maxValue) {
        ByteBuffer in = buildBuffer(4,5, minValue, maxValue);
        formatBuffer(in, 4,5, minValue, maxValue);
        return bind(4, in);
    }

    private Table bind(int numColumns, ByteBuffer in) {
        Result<Table, BufferFailure> result = factory.bindPopulatedBuffer(numColumns, in);
        assertTrue(result.isOk());
        return result.get();
    }

    private void formatBuffer(ByteBuffer buffer, int numColumns, int numRows, int minValue,int maxValue) {
        Range valueRange = new Range(minValue, maxValue);
        factory.formatAndBindEmptyBuffer(numColumns, numRows, valueRange, buffer);
    }

    private ByteBuffer buildBuffer(int numColumns, int numRows, int minValue,int maxValue) {
        Range valueRange = new Range(minValue, maxValue);
        int bufferSize = factory.calculateRequiredBufferSize(numColumns, numRows, valueRange);
        return ByteBuffer.allocate(bufferSize);
    }


    private Table buildTable(int numColumns, int numRows, int minValue,int maxValue) {
        Range valueRange = new Range(minValue, maxValue);
        int bufferSize = factory.calculateRequiredBufferSize(numColumns, numRows, valueRange);
        ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
        return factory.formatAndBindEmptyBuffer(numColumns, numRows, valueRange, buffer);
    }
}
