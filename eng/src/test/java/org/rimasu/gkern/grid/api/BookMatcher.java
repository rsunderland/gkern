package org.rimasu.gkern.grid.api;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeMatcher;

import java.util.Iterator;


/**
 * Matcher that checks that a row cursor has all the expected values.
 * Null arguments are expected to align with clear cells.
 */
public class BookMatcher extends TypeSafeMatcher<Book> {

    private final Book expectedValue;

    private BookMatcher(Book expectedValue) {
        this.expectedValue = expectedValue;
    }

    @Override
    public boolean matchesSafely(Book actual) {
        if(actual.getNumTables() == expectedValue.getNumTables()) {
            Iterator<Table> actualTables = actual.iterator();
            for (Table anExpectedValue : expectedValue) {
                if (actualTables.hasNext()) {
                    Table expectedTable = anExpectedValue;
                    Table actualTable = actualTables.next();
                    if (!tablesMatch(expectedTable, actualTable)) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private boolean tablesMatch(Table actual, Table expected) {
        if(actual.getNumColumns() != expected.getNumColumns())
        {
            return false;
        }

        if(actual.getNumRows() != expected.getNumRows()) {
            return false;
        }

        RowCursor actualCursor = actual.createCursor();
        RowCursor expectedCursor = expected.createCursor();

        while(actualCursor.moveToNextRow() && expectedCursor.moveToNextRow()) {
            int numColumns = expected.getNumColumns();
            if(!rowsMatch(numColumns, actualCursor, expectedCursor)) {
                return false;
            }
        }

        return true;
    }

    private boolean rowsMatch(int numColumns, RowCursor actualCursor, RowCursor expectedCursor) {
        for(int i=1;i<=numColumns;i++) {
            if(expectedCursor.isClear(i)) {
                if(!actualCursor.isClear(i)) {
                    return false;
                }
            } else if(expectedCursor.getValue(i) != actualCursor.getValue(i)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void describeTo(Description descr) {
        descr.appendValue(expectedValue);
    }

    @Factory
    public static BookMatcher matchesBook(Book expectedValue) {
        return new BookMatcher(expectedValue);
    }
}