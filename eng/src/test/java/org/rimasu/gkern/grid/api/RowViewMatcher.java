package org.rimasu.gkern.grid.api;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeMatcher;


/**
 * Matcher that checks that a row cursor has all the expected values.
 * Null arguments are expected to align with clear cells.
 */
public class RowViewMatcher extends TypeSafeMatcher<RowView> {

    private final Integer[] expectedValues;

    private RowViewMatcher(Integer... expectedValues) {
        this.expectedValues = expectedValues;
    }

    @Override
    public boolean matchesSafely(RowView actual) {
        int valuePosition = 1;
        for(Integer expectedValue : expectedValues) {
            if(expectedValue == null) {
                if(!actual.isClear(valuePosition))
                    return false;
            } else {
                if(actual.getValue(valuePosition) != expectedValue)
                    return false;
            }
            valuePosition++;
        }
        return true;
    }

    @Override
    public void describeTo(Description descr) {
        for(Integer expectedValue : expectedValues) {
            if(expectedValue == null) {
                descr.appendText("?");
            } else {
                descr.appendText(Integer.toString(expectedValue));
            }
            descr.appendText(" ");
        }
    }

    @Factory
    public static RowViewMatcher hasValues(Integer... expectedValues) {
        return new RowViewMatcher(expectedValues);
    }
}