package org.rimasu.gkern.grid.buf;

import org.junit.Test;
import org.rimasu.gkern.grid.api.KernelErrorCode;
import org.rimasu.gkern.grid.api.KernelException;
import org.rimasu.gkern.grid.api.Table;
import org.rimasu.gkern.grid.api.TableTest;
import org.rimasu.gkern.op.api.Result;

import java.nio.ByteBuffer;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.rimasu.gkern.grid.api.KernelErrorCode.BUFFER_TABLE_HAS_BAD_SIGNATURE_BYTE;
import static org.rimasu.gkern.grid.buf.BufferFailure.TABLE_BUFFER_TOO_LARGE;
import static org.rimasu.gkern.grid.buf.BufferFailure.TABLE_BUFFER_TOO_SMALL;


public abstract class InnerBufferTableFactoryTest extends TableTest {

    private static final byte BAD_SIGNATURE = (byte) 0;

    protected abstract InnerBufferTableFactory createFactory();

    @Override
    protected Table createTable(int numColumns, int numRows) {
        InnerBufferTableFactory factory = createFactory();
        int bufferSize = factory.calculateRequiredBufferSize(numColumns, numRows);
        ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
        return factory.formatAndBindEmptyBuffer(numColumns, numRows, buffer);
    }


    @Test
    public void canBuildTwoTablesThatShareTheSameBuffer() {

        InnerBufferTableFactory factory = createFactory();
        int bufferSize = factory.calculateRequiredBufferSize(2, 3);

        ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
        Table table1 = factory.formatAndBindEmptyBuffer(2, 3, buffer);
        Result<Table, BufferFailure> result2 = factory.bindPopulatedBuffer(2, buffer);

        assertTrue(result2.isOk());

        Table table2 = result2.get();

        assertThat(table2.getNumColumns(), is(2));
        assertThat(table2.getNumRows(), is(3));

        table1.createCursor().moveToRow(1).setValue(1, 4);
        table1.createCursor().moveToRow(3).setValue(2, 5);

        assertThat(table2.createCursor().moveToRow(1).getValue(1), is(4));
        assertThat(table2.createCursor().moveToRow(3).getValue(2), is(5));
    }

    @Test
    public void returnsBufferToSmallDuringBindEmptyIfBufferJustTooSmall() {

        InnerBufferTableFactory factory = createFactory();
        int bufferSize = factory.calculateRequiredBufferSize(2, 3);

        ByteBuffer tooSmallBuffer = ByteBuffer.allocate(bufferSize - 1);

        try {
            factory.formatAndBindEmptyBuffer(2, 3, tooSmallBuffer);
            fail();
        } catch (KernelException e) {
            assertThat(e.getCode(), is(KernelErrorCode.EMPTY_BUFFER_INCORRECT_SIZE));
        }
    }

    @Test
    public void returnsBufferToSmallDuringBindEmptyIfBufferEmpty() {

        InnerBufferTableFactory factory = createFactory();


        ByteBuffer tooSmallBuffer = ByteBuffer.allocate(0);

        try {
            factory.formatAndBindEmptyBuffer(2, 3, tooSmallBuffer);
            fail();
        } catch (KernelException e) {
            assertThat(e.getCode(), is(KernelErrorCode.EMPTY_BUFFER_INCORRECT_SIZE));
        }
    }

    @Test
    public void returnsBufferToSmallDuringBindEmptyIfBufferJustTooLarge() {

        InnerBufferTableFactory factory = createFactory();

        int bufferSize = factory.calculateRequiredBufferSize(2, 3);

        ByteBuffer tooSmallBuffer = ByteBuffer.allocate(bufferSize + 1);

        try {
            factory.formatAndBindEmptyBuffer(2, 3, tooSmallBuffer);
            fail();
        } catch (KernelException e) {
            assertThat(e.getCode(), is(KernelErrorCode.EMPTY_BUFFER_INCORRECT_SIZE));
        }
    }


    @Test
    public void returnsBufferToSmallDuringBindPopulatedIfBufferJustTooSmall() {

        InnerBufferTableFactory factory = createFactory();

        int bufferSize = factory.calculateRequiredBufferSize(2, 3);

        ByteBuffer buffer = ByteBuffer.allocate(bufferSize);

        factory.formatAndBindEmptyBuffer(2, 3, buffer);

        buffer.limit(buffer.limit() - 1);

        Result<Table, BufferFailure> result = factory.bindPopulatedBuffer(2, buffer);

        assertTrue(result.isFailure());
        assertThat(result.getFailureDetails(), is(TABLE_BUFFER_TOO_SMALL));
    }

    @Test
    public void returnsBufferToSmallDuringBindPopulatedIfBufferEmpty() {

        InnerBufferTableFactory factory = createFactory();
        int bufferSize = factory.calculateRequiredBufferSize(2, 3);

        ByteBuffer buffer = ByteBuffer.allocate(bufferSize);

        factory.formatAndBindEmptyBuffer(2, 3, buffer);

        buffer.limit(0);

        Result<Table, BufferFailure> result = factory.bindPopulatedBuffer(2, buffer);

        assertTrue(result.isFailure());
        assertThat(result.getFailureDetails(), is(TABLE_BUFFER_TOO_SMALL));
    }

    @Test
    public void returnsBufferToSmallDuringBindPopulatedIfBufferJustTooLarge() {

        InnerBufferTableFactory factory = createFactory();
        int bufferSize = factory.calculateRequiredBufferSize(2, 3);

        ByteBuffer buffer = ByteBuffer.allocate(bufferSize + 1);

        buffer.limit(buffer.limit() - 1);

        factory.formatAndBindEmptyBuffer(2, 3, buffer);

        buffer.limit(buffer.capacity());

        Result<Table, BufferFailure> result = factory.bindPopulatedBuffer(2, buffer);

        assertTrue(result.isFailure());
        assertThat(result.getFailureDetails(), is(TABLE_BUFFER_TOO_LARGE));
    }

    @Test
    public void throwsExceptionWhenBindingBufferWithBadSignatureByte() {

        InnerBufferTableFactory factory = createFactory();
        int bufferSize = factory.calculateRequiredBufferSize(2, 3);

        ByteBuffer buffer = ByteBuffer.allocate(bufferSize);

        factory.formatAndBindEmptyBuffer(2, 3, buffer);

        buffer.put(InnerBufferTableFactory.SIGNATURE_BYTE_OFFSET, BAD_SIGNATURE);

        try {
            factory.bindPopulatedBuffer(2, buffer);
            fail();
        } catch (KernelException e) {
            assertThat(e.getCode(), is(BUFFER_TABLE_HAS_BAD_SIGNATURE_BYTE));
        }

    }


}

