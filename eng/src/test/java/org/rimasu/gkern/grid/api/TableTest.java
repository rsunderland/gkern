package org.rimasu.gkern.grid.api;


import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.rimasu.gkern.grid.api.KernelErrorCode.*;
import static org.rimasu.gkern.grid.api.RowViewMatcher.hasValues;

public abstract class TableTest {

    private static final int NUM_ROWS = 2;
    private static final int NUM_COLUMNS = 2;

    private Table table;
    private RowCursor cursor;

    protected abstract Table createTable(int numColumns, int numRows);

    @Before
    public void createTableAndCursor() {
        table = createTable(NUM_COLUMNS, NUM_ROWS);
        cursor = table.createCursor();
    }

    @Test
    public void canSetAndGetValues() {
        setRowValues(1, 1, 2);
        setRowValues(2, 3, 4);
        assertThat(cursor.moveToRow(1), hasValues(1, 2));
        assertThat(cursor.moveToRow(2), hasValues(3, 4));
    }

    @Test
    public void canGetRange() {
        setRowValues(1, 1, 2);
        setRowValues(2, 3, 4);
        assertThat(table.getValueRange(),is(new Range(1,4)));
    }

    @Test
    public void canSetAndClearValues() {
        setRowValues(1, null, 2);
        setRowValues(2, 3, null);

        assertThat(cursor.moveToRow(1), hasValues(null, 2));
        assertThat(cursor.moveToRow(2), hasValues(3, null));
    }

    @Test
    public void canSetValuesInRow() {
        setRowValues(1, null, 2);
        setRowValues(2, 3, null);

        row(1).copyValues(row(2));
        assertThat(row(1),hasValues(3, null));
    }

    @Test
    public void newlyCreateCursorIsOnRowBeforeTable() {
        assertThat(cursor.getRowNumber(), is(0));
    }

    @Test
    public void canTraverseTableForwards() {
        setRowValues(1, 1, 2);
        setRowValues(2, 3, 4);

        // starts before table.
        assertTrue(onRow(0));

        // moves to first row.
        assertTrue(cursor.moveToNextRow());
        assertTrue(onRow(1));
        assertThat(cursor, hasValues(1, 2));

        // moves to second row.
        assertTrue(cursor.moveToNextRow());
        assertTrue(onRow(2));
        assertThat(cursor, hasValues(3, 4));

        // moving beyond end of table returns false.
        assertFalse(cursor.moveToNextRow());
        assertTrue(onRow(3));

        // moving even further has no effect
        assertFalse(cursor.moveToNextRow());
        assertTrue(onRow(3));
    }

    @Test
    public void canTraverseTableBackwards() {
        setRowValues(1, 1, 2);
        setRowValues(2, 3, 4);

        cursor.moveToRow(3);

        // starts after table.
        assertTrue(onRow(3));

        // moves to second row.
        assertTrue(cursor.moveToPrevRow());
        assertTrue(onRow(2));
        assertThat(cursor, hasValues(3, 4));

        // moves to first row.
        assertTrue(cursor.moveToPrevRow());
        assertTrue(onRow(1));
        assertThat(cursor, hasValues(1, 2));

        // moving before start of table returns false.
        assertFalse(cursor.moveToPrevRow());
        assertTrue(onRow(0));

        // moving even further has no effect
        assertFalse(cursor.moveToPrevRow());
        assertTrue(onRow(0));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void canNotMoveMoreThanOneRowBeforeStart() {
        cursor.moveToRow(-1);
    }

    @Test
    public void canMoveToOneRowBeyondEndOfTable() {
        cursor.moveToRow(NUM_ROWS + 1);
        assertThat(cursor.getRowNumber(), is(NUM_ROWS + 1));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void canNotMoveToMoreThanOneRowBeyondEndOfTable() {
        cursor.moveToRow(NUM_ROWS + 2);
    }

    @Test
    public void clearSortsBeforeAnyValue() {
        setRowValues(1, null, 0);
        setRowValues(2, 0, 0);
        assertTrue(row(1).compareValuesTo(row(2)) < 0);
        assertTrue(row(2).compareValuesTo(row(1)) > 0);
    }

    @Test
    public void firstValueTrumpsSecond() {
        setRowValues(1, 1, 2);
        setRowValues(2, 2, 1);
        assertTrue(row(1).compareValuesTo(row(2)) < 0);
        assertTrue(row(2).compareValuesTo(row(1)) > 0);
    }

    @Test
    public void secondValueUsedIfFirstTheSame() {
        setRowValues(1, 1, 1);
        setRowValues(2, 1, 2);
        assertTrue(row(1).compareValuesTo(row(2)) < 0);
        assertTrue(row(2).compareValuesTo(row(1)) > 0);
    }

    @Test
    public void truncatingTableChangesNumRows() {
        setRowValues(1, 1, 1);
        setRowValues(2, 1, 2);

        table.truncateTo(1);

        assertThat(table.getNumRows(), is(1));
    }

    @Test
    public void truncatingToZeroReturnsEmptyView() {

        setRowValues(1, 1, 1);
        setRowValues(2, 1, 2);

        table.truncateTo(0);
        assertThat(table.getNumRows(), is(0));
    }

    @Test
    public void canNotTruncateToNegativeNumRows() {

        setRowValues(1, 1, 1);
        setRowValues(2, 1, 2);

        try {
            table.truncateTo(-1);
        } catch (KernelException e) {
            assertThat(e.getCode(), is(TABLE_HAS_NEGATIVE_NUMBER_OF_ROWS));
        }
    }

    @Test
    public void canNotTruncateToHigherNumberOfRows() {
        setRowValues(1, 1, 1);
        setRowValues(2, 1, 2);

        try {
            table.truncateTo(3);
            fail();
        } catch (KernelException e) {
            assertThat(e.getCode(), is(TABLE_CAN_NOT_BE_EXTENDED));
        }
    }

    @Test
    public void canTruncateATruncatedTable() {
        setRowValues(1, 1, 1);
        setRowValues(2, 1, 2);
        table.truncateTo(1);
        table.truncateTo(0);
        assertThat(table.getNumRows(), is(0));
    }

    @Test
    public void truncatingTableDoesNotAffectExistingCursors() {
        setRowValues(1, 1, 4);
        setRowValues(2, 2, 5);
        RowCursor c = table.createCursor();

        table.truncateTo(1);

        assertThat(c.getRowNumber(), is(0));
        assertTrue(c.moveToNextRow());

        assertThat(c.getRowNumber(), is(1));
        assertThat(c, hasValues(1, 4));

        assertTrue(c.moveToNextRow());

        assertThat(c.getRowNumber(), is(2));
        assertThat(c, hasValues(2, 5));

        assertFalse(c.moveToNextRow());
        assertFalse(c.moveToNextRow());

        assertThat(c.getRowNumber(), is(3));
    }

    @Test
    public void truncatingTableDoesAffectSubsequentCursors() {
        setRowValues(1, 1, 4);
        setRowValues(2, 2, 5);


        table.truncateTo(1);
        RowCursor c = table.createCursor();

        assertThat(c.getRowNumber(), is(0));
        assertTrue(c.moveToNextRow());

        assertThat(c.getRowNumber(), is(1));
        assertThat(c, hasValues(1, 4));

        assertFalse(c.moveToNextRow());
        assertFalse(c.moveToNextRow());

        assertThat(c.getRowNumber(), is(2));
    }


    protected RowCursor row(int rowNumber) {
        return table.createCursor().moveToRow(rowNumber);
    }

    private void setRowValues(int rowNumber, Integer... values) {

        RowCursor row = row(rowNumber);
        int position = 1;
        for (Integer value : values) {
            if (value == null) {
                row.clearValue(position);
            } else {
                row.setValue(position, value);
            }
            position++;
        }
    }

    private boolean onRow(int rowNumber) {
        return cursor.getRowNumber() == rowNumber;
    }

}
