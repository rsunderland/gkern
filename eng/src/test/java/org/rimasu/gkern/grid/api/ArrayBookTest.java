package org.rimasu.gkern.grid.api;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ArrayBookTest {

    @Mock
    private Table table1;

    @Mock
    private Table table2;

    private ArrayBook book;


    @Before
    public void createBook() {

        Collection<Table> tables = Arrays.asList(table1, table2);

        when(table1.getNumRows()).thenReturn(1);
        when(table2.getNumRows()).thenReturn(4);

        book = new ArrayBook(tables);
    }

    @Test
    public void canGetTables() {
        assertThat(book, contains(table1, table2));
    }

    @Test
    public void canGetNumTables() {
        assertThat(book.getNumTables(),is(2));
    }
}
