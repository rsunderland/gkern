package org.rimasu.gkern.grid.api;


import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

public class TableShapeTest {

    @Test
    public void canGetMaxRows() {
        assertThat(tableShape(10).getMaxRows(),is(10));
    }

    @Test
    public void canGetNumberOfColumns() {
        TableShape shape = tableShape(10, columnShape(1, 4), columnShape(4, 10));
        assertThat(shape.getNumColumns(),is(2));
    }

    @Test
    public void canGetColumnShape() {
        TableShape shape = tableShape(10, columnShape(1, 4), columnShape(4, 10));
        assertThat(shape.getColumnShape(1),is(columnShape(1, 4)));
        assertThat(shape.getColumnShape(2),is(columnShape(4, 10)));
    }

    @Test
    public void equalsAndHashCodeSensitiveToMaxRows() {
        TableShape a = tableShape(10);
        TableShape b = tableShape(12);

        assertThat(a,is(tableShape(10)));
        assertThat(a,is(a));
        assertThat(a,is(not(b)));

        assertThat(a.hashCode(),is(tableShape(10).hashCode()));
        assertThat(a.hashCode(),is(not(b.hashCode())));
    }

    @Test
    public void equalsAndHashCodeSensitiveToColumnShapes() {
        TableShape a = tableShape(10, columnShape(1, 4));
        TableShape b = tableShape(10, columnShape(1, 5));

        assertThat(a,is(tableShape(10, columnShape(1, 4))));
        assertThat(a,is(a));
        assertThat(a,is(not(b)));

        assertThat(a.hashCode(),is(tableShape(10, columnShape(1, 4)).hashCode()));
        assertThat(a.hashCode(),is(not(b.hashCode())));
    }

    @Test
    public void toStringTest() {
        TableShape shape = tableShape(10, columnShape(1, 4), columnShape(4, 10));
        assertThat(shape.toString(),is("(10,[(1,4)(4,10)])"));
    }

    private TableShape tableShape(int maxRows, ColumnShape... columnRanges) {
        return new TableShape(maxRows, columnRanges);
    }

    private ColumnShape columnShape(int minInclusive, int maxInclusive) {
        return new ColumnShape(new Range(minInclusive, maxInclusive));
    }
}
