package org.rimasu.gkern.grid.array;

import org.rimasu.gkern.grid.api.Table;
import org.rimasu.gkern.grid.api.TableTest;

public class IntArrayTableTest extends TableTest {

    @Override
    protected Table createTable(int numColumns, int numRows) {
        return new IntArrayTable(numColumns, numRows);
    }

}
