package org.rimasu.gkern.grid.api;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeMatcher;


/**
 * Check that hash codes match.
 */
public class HashCodeMatcher extends TypeSafeMatcher<Object> {

    private final Object expected;

    private HashCodeMatcher(Object expectedValues) {
        this.expected = expectedValues;
    }

    @Override
    public boolean matchesSafely(Object actual) {
       return  actual.hashCode() == expected.hashCode();
    }

    @Override
    public void describeTo(Description descr) {

        descr.appendText(Integer.toString(expected.hashCode()));
    }

    @Factory
    public static HashCodeMatcher hasSameHashCodeAs(Object expectedValues) {
        return new HashCodeMatcher(expectedValues);
    }
}