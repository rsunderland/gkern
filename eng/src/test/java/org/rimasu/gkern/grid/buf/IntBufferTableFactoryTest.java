package org.rimasu.gkern.grid.buf;


public class IntBufferTableFactoryTest extends InnerBufferTableFactoryTest {

    @Override
    protected InnerBufferTableFactory createFactory() {
        return new IntBufferTableFactory();
    }
}
