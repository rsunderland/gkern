package org.rimasu.gkern.grid.buf;

import org.junit.Test;

import java.nio.ByteBuffer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


public class FrameTest
{
    @Test
    public void canCreateFrameWithNoBuckets() {

        Frame frame = frame();

        assertThat(frame.getNumBuckets(),is(0));

        ByteBuffer backing = frame.getBackingBuffer();
        assertThat(backing.remaining(),is(2));
    }

    @Test
    public void canBuildFrameWithNoBucketsFromExistingBuffer() {

        ByteBuffer raw = frame().getBackingBuffer();

        Frame frameOut = new Frame(raw);

        assertThat(frameOut.getNumBuckets(),is(0));
    }

    @Test
    public void canBuildFrameWithSingleEmptyBucket(){
        Frame frame = frame(0);
        assertThat(frame.getNumBuckets(),is(1));
        ByteBuffer bucket = frame.getBucket(1);
        assertThat(bucket.remaining(),is(0));
    }

    @Test
    public void canBuildFrameWithSingleEmptyBucketFromExistingBuffer(){
        ByteBuffer raw = frame(0).getBackingBuffer();
        Frame frameOut = new Frame(raw);
        ByteBuffer bucket = frameOut.getBucket(1);
        assertThat(bucket.remaining(),is(0));
    }

    @Test
    public void canBuildFrameWithMultipleNonEmptyBuckets() {
        Frame frame = frame(2,8,1002);
        assertThat(frame.getNumBuckets(),is(3));
        assertThat(frame.getBucket(1).remaining(),is(2));
        assertThat(frame.getBucket(2).remaining(),is(8));
        assertThat(frame.getBucket(3).remaining(),is(1002));
    }

    @Test
    public void canBuildFrameWithMultipleNonEmptyBucketsFromExistingBuffer() {
        ByteBuffer raw = frame(2,8,1002).getBackingBuffer();
        Frame frame = new Frame(raw);
        assertThat(frame.getNumBuckets(),is(3));
        assertThat(frame.getBucket(1).remaining(),is(2));
        assertThat(frame.getBucket(2).remaining(),is(8));
        assertThat(frame.getBucket(3).remaining(),is(1002));
    }


    private Frame frame(int... bucketSizes) {
        return new Frame(bucketSizes);
    }
}
