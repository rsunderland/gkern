package org.rimasu.gkern.grid.array;

import org.junit.Before;
import org.junit.Test;
import org.rimasu.gkern.grid.api.*;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class IntArrayTableFactoryTest {

    private IntArrayTableFactory factory;

    @Before
    public void createBookFactory() {
        factory = new IntArrayTableFactory();
    }

    @Test
    public void canCreateEmptyTable() {
        Table table = factory.createTable(0, 0);
        assertThat(table.getNumColumns(),is(0));
        assertThat(table.getNumRows(),is(0));
    }

    @Test
    public void canCreateMultiColumnMultiRowTable() {
        Table table = factory.createTable(2, 4);

        assertThat(table.getNumColumns(),is(2));
        assertThat(table.getNumRows(),is(4));
    }
}
