package org.rimasu.gkern.grid.cursors;


import org.junit.Test;
import org.rimasu.gkern.grid.api.RowCursor;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class SymDifferenceCursorTest extends DualForwardRowCursorTest<SymDifferenceCursor>{

    @Test
    public void canSymDiffDisjointTables() {
        givenLeftTable(1, 2, 5, 6);
        givenRightTable(3, 4, 7, 8);
        whenTraversed();
        assertResultIs(1, 2, 5, 6);
        assertThat(source.getIgnoredCount(),is(4));
    }



    @Test
    public void canSymDiffIdenticalTables() {
        givenLeftTable(1, 3, 5, 7);
        givenRightTable(1, 3, 5, 7);
        whenTraversed();
        assertResultIs();
        assertThat(source.getIgnoredCount(),is(0));
    }

    @Test
    public void canSymDiffPartiallyDisjointTables() {
        givenLeftTable(1, 4, 5, 7);
        givenRightTable(1, 3, 6, 7);
        whenTraversed();
        assertResultIs(4, 5);
        assertThat(source.getIgnoredCount(), is(2));
    }

    @Test
    public void canSymDiffWhenLeftIsEmpty() {
        givenLeftTable();
        givenRightTable(1, 3, 5, 7);
        whenTraversed();
        assertResultIs();
        assertThat(source.getIgnoredCount(),is(4));
    }

    @Test
    public void canSymDiffWhenRightIsEmpty() {
        givenLeftTable(1, 3, 5, 7);
        givenRightTable();
        whenTraversed();
        assertResultIs(1, 3, 5, 7);
        assertThat(source.getIgnoredCount(),is(0));
    }

    @Test
    public void canUnionWhenBothAreEmpty() {
        givenLeftTable();
        givenRightTable();
        whenTraversed();
        assertResultIs();
        assertThat(source.getIgnoredCount(),is(0));
    }

    @Override
    protected SymDifferenceCursor createCursor(RowCursor left, RowCursor right) {
        return new SymDifferenceCursor(left, right);
    }
}
