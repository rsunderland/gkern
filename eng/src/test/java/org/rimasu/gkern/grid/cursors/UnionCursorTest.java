package org.rimasu.gkern.grid.cursors;


import org.junit.Test;
import org.rimasu.gkern.grid.api.RowCursor;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class UnionCursorTest extends DualForwardRowCursorTest<UnionCursor> {

    @Test
    public void canUnionDisjointTables() {
        givenLeftTable(1, 2, 5, 6);
        givenRightTable(3, 4, 7, 8);
        whenTraversed();
        assertResultIs(1, 2, 3, 4, 5, 6, 7, 8);
    }

    @Test
    public void canUnionDisjointWhenLeftIsLonger() {
        givenLeftTable(1, 2, 5, 6, 9, 10);
        givenRightTable(3, 4, 7, 8);
        whenTraversed();
        assertResultIs(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    }

    @Test
    public void canUnionDisjointWhenRightIsLonger() {
        givenLeftTable(1, 2, 5, 6);
        givenRightTable(3, 4, 7, 8, 9, 10);
        whenTraversed();
        assertResultIs(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    }

    @Test
    public void canUnionIdenticalTables() {
        givenLeftTable(1, 3, 5, 7);
        givenRightTable(1, 3, 5, 7);
        whenTraversed();
        assertResultIs(1, 3, 5, 7);
    }

    @Test
    public void canUnionPartiallyDisjointTables() {
        givenLeftTable(1, 4, 5, 7);
        givenRightTable(1, 3, 5, 7);
        whenTraversed();
        assertResultIs(1, 3, 4, 5, 7);
    }

    @Test
    public void canUnionWhenLeftIsEmpty() {
        givenLeftTable();
        givenRightTable(1, 3, 5, 7);
        whenTraversed();
        assertResultIs(1, 3, 5, 7);
    }

    @Test
    public void canUnionWhenRightIsEmpty() {
        givenLeftTable(1, 3, 5, 7);
        givenRightTable();
        whenTraversed();
        assertResultIs(1, 3, 5, 7);
    }

    @Test
    public void canUnionWhenBothAreEmpty() {
        givenLeftTable();
        givenRightTable();
        whenTraversed();
        assertResultIs();
    }

    @Test
    public void canGetSizeOfIntersection() {
        givenLeftTable(1, 3, 5, 7);
        givenRightTable(1, 5, 8);

        whenTraversed();
        assertResultIs(1,3,5,7,8);

        assertThat(source.getIntersectionCount(),is(2));
    }

    @Override
    protected UnionCursor createCursor(RowCursor left, RowCursor right) {
        return new UnionCursor(left, right);
    }
}
