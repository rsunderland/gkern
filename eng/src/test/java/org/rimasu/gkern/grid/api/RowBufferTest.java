package org.rimasu.gkern.grid.api;


import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.rimasu.gkern.grid.api.RowViewMatcher.hasValues;

public class RowBufferTest {

    @Test
    public void getAndSetValues() {
        assertThat(row(1,2,3), hasValues(1, 2, 3));
    }

    @Test
    public void clearAndSet() {
        assertThat(row(4,null,13), hasValues(4, null, 13));
    }

    @Test
    public void canSetValuesFromARowView() {
        RowBuffer a  = new RowBuffer(3);
        a.copyValues(row(3, null, 8));
        assertThat(a, hasValues(3,null,8));
    }

    @Test
    public void canCompareValues() {
        assertThat(row(2).compareValuesTo(row(2)),is(0));
        assertThat(row(3).compareValuesTo(row(2)),is(1));
        assertThat(row(2).compareValuesTo(row(3)),is(-1));

        assertThat(row(1,2).compareValuesTo(row(1,2)),is(0));
        assertThat(row(1,3).compareValuesTo(row(1,2)),is(1));
        assertThat(row(1,2).compareValuesTo(row(1,3)),is(-1));
    }


    private RowView row(Integer... values) {
        RowView buf = new RowBuffer(values.length);
        int i=1;
        for(Integer value : values) {
            if(value == null) {
                buf.clearValue(i);
            } else {
                buf.setValue(i, value);
            }
            i++;
        }
        return buf;
    }
}
