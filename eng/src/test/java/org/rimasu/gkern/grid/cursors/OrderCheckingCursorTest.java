package org.rimasu.gkern.grid.cursors;


import org.junit.Before;
import org.junit.Test;
import org.rimasu.gkern.grid.api.*;
import org.rimasu.gkern.grid.array.IntArrayTable;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.rimasu.gkern.grid.api.RowViewMatcher.hasValues;

public class OrderCheckingCursorTest {

    private static final int NUM_ROWS = 2;
    private Table table;
    private RowCursor delegate;
    private OrderCheckingCursor cursor;

    @Before
    public void createTableAndCursor() {
        table = createTable(2, NUM_ROWS);
        delegate = table.createCursor();
        cursor = new OrderCheckingCursor(2, delegate);
    }

    Table createTable(int numColumns, int numRows) {
        return new IntArrayTable(numColumns, numRows);
    }

    @Test
    public void canSetAndGetValues() {
        setRowValues(1, 1, 2);
        setRowValues(2, 3, 4);
        assertTrue(cursor.moveToNextRow());
        assertThat(cursor, hasValues(1,2));
        assertTrue(cursor.moveToNextRow());
        assertThat(cursor, hasValues(3,4));
        assertFalse(cursor.moveToNextRow());
    }

    @Test
    public void canDetectIfValuesAreFullyOrdered() {
        setRowValues(1, 1, 4);
        setRowValues(2, 3, 2);

        scanTable();

        assertTrue(cursor.isFullyOrdered());
    }

    @Test
    public void canDetectIfSecondValuesAreNotOrdered() {
        setRowValues(1, 1, 2);
        setRowValues(2, 1, 1);

        scanTable();

        assertFalse(cursor.isFullyOrdered());
    }

    @Test
    public void canDetectIfFirstValuesAreNotOrdered() {
        setRowValues(1, 2, 1);
        setRowValues(2, 1, 2);

        scanTable();

        assertFalse(cursor.isFullyOrdered());
    }

    @SuppressWarnings("PMD")
    private void scanTable() {
        while(cursor.moveToNextRow()) {
            // Nothing to do
        }
    }

    private void setRowValues(int rowNumber, Integer... values) {

        RowCursor row = row(rowNumber);
        int position = 1;
        for(Integer value : values) {
            if(value == null) {
                row.clearValue(position);
            } else {
                row.setValue(position, value);
            }
            position++;
        }
    }

    private RowCursor row(int rowNumber) {
        return table.createCursor().moveToRow(rowNumber);
    }
}
