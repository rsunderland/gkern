package org.rimasu.gkern.grid.buf;

import org.junit.Test;
import org.rimasu.gkern.grid.api.KernelErrorCode;
import org.rimasu.gkern.grid.api.KernelException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.fail;

public class ByteBufferTableTest extends InnerBufferTableFactoryTest {

    @Override
    protected InnerBufferTableFactory createFactory() {
        return new ByteBufferTableFactory();
    }

    @Test
    public void canStoreMaxShort() {
        row(1).setValue(1, Byte.MAX_VALUE);
        assertThat(row(1).getValue(1), is((int)Byte.MAX_VALUE));
    }

    @Test
    public void exceptionThrownIfStoringValueGreaterThanMaxShort() {
        try {
            row(1).setValue(1, Byte.MAX_VALUE + 1);
            fail();
        } catch (KernelException e) {
            assertThat(e.getCode(), is(KernelErrorCode.VALUE_OVERFLOW));
        }
    }


    @Test
    public void canStoreMinShortPlus1() {
        row(1).setValue(1, Byte.MIN_VALUE + 1);
        assertThat(row(1).getValue(1), is(Byte.MIN_VALUE + 1));
    }

    @Test
    public void exceptionThrownIfStoringValueLessThanOrEqualToMinShort() {
        try {
            row(1).setValue(1, (int)Byte.MIN_VALUE );
            fail();
        } catch (KernelException e) {
            assertThat(e.getCode(), is(KernelErrorCode.VALUE_UNDERFLOW));
        }
    }


}
