package org.rimasu.gkern.grid.cursors;


import org.rimasu.gkern.grid.api.*;
import org.rimasu.gkern.grid.array.IntArrayTable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public abstract class DualForwardRowCursorTest<C extends ForwardRowCursor> {

    private Table leftTable;
    private Table rightTable;
    private List<Integer> actualValues;

    C source;

    protected abstract C createCursor(RowCursor left, RowCursor right);


    void givenLeftTable(Integer... values) {
        leftTable =  table(values);
    }

    void givenRightTable(Integer... values) {
        rightTable =  table(values);
    }

    private Table table(Integer[] values) {
        Table table = new IntArrayTable(1, values.length);
        RowCursor cursor = table.createCursor();
        for(Integer value : values) {
            cursor.moveToNextRow();
            if(value == null) {
                cursor.clearValue(1);
            } else {
                cursor.setValue(1, value);
            }
        }
        return table;
    }

    void whenTraversed() {
        actualValues = new ArrayList<>();
        source = createCursor(leftTable.createCursor(), rightTable.createCursor());
        while(source.moveToNextRow()) {
          if(source.isClear(1)) {
              actualValues.add(null);
          } else {
              actualValues.add(source.getValue(1));
          }
        }
    }

    void assertResultIs(Integer... values) {
        assertThat(actualValues,is(Arrays.asList(values)));
    }

    private static TableShape tableShape(int maxRows, ColumnShape... columnRanges) {
        return new TableShape(maxRows, Arrays.asList(columnRanges));
    }

    private static ColumnShape columnShape(int minInclusive, int maxInclusive) {
        return new ColumnShape(new Range(minInclusive, maxInclusive));
    }

}
