package org.rimasu.gkern.grid.api;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class RangeTest {

    @Test
    public void canGetMinInclusive() {
        assertThat(range(-10, 10).getMinInclusive(),is(-10));
    }

    @Test
    public void canGetMaxInclusive() {
        assertThat(range(-10, 10).getMaxInclusive(),is(10));
    }

    @Test
    public void canTestWhetherItemIsInRange() {
        assertTrue(range(10,10).contains(10));
        assertFalse(range(3, 5).contains(2));
        assertTrue(range(3,5).contains(3));
        assertTrue(range(3,5).contains(4));
        assertTrue(range(3,5).contains(5));
        assertFalse(range(3, 5).contains(6));
    }

    @Test
    public void canTestWhetherRangeContainsRange() {
        assertTrue(range(5,10).contains(range(5,10)));
        assertTrue(range(5,10).contains(range(6,10)));
        assertTrue(range(5,10).contains(range(6,9)));
        assertFalse(range(5,10).contains(range(4, 10)));
        assertFalse(range(5, 10).contains(range(6, 11)));
    }

    @Test
    public void canUnionRanges() {
        assertThat(range(1,4).unionWith(range(7, 10)),is(range(1, 10)));
        assertThat(range(7,10).unionWith(range(1, 4)),is(range(1, 10)));
        assertThat(range(1,10).unionWith(range(4, 7)),is(range(1, 10)));
    }

    @Test
    public void canUnionRangesWithValue() {
        assertThat(range(1,6).unionWith(-10),is(range(-10, 6)));
        assertThat(range(1,6).unionWith(3),is(range(1, 6)));
        assertThat(range(1,6).unionWith(10),is(range(1, 10)));
    }

    @Test
    public void testEquals() {
        assertThat(range(1, 3),is(range(1,3)));
        assertThat(range(1, 3),is(not(range(1, 4))));
        assertThat(range(1, 3),is(not(range(2, 3))));
        assertThat(range(1, 3),is(not(range(2, 4))));
    }

    @Test
    public void testHashCode() {
        assertThat(range(1, 3).hashCode(), is(range(1, 3).hashCode()));
        assertThat(range(1, 3).hashCode(), is(not(range(1, 4).hashCode())));
        assertThat(range(1, 3).hashCode(), is(not(range(2, 3).hashCode())));
        assertThat(range(1, 3).hashCode(), is(not(range(2, 4).hashCode())));
    }

    @Test
    public void testToString() {
        assertThat(range(1, 3).toString(),is("(1,3)"));
    }

    private Range range(int minInclusive, int maxInclusive) {
        return new Range(minInclusive, maxInclusive);
    }
}
