package org.rimasu.gkern.grid.api;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;


public class ColumnShapeTest {

    @Test
    public void canRange() {
        assertThat(columnShape(-10, 10).getRange(),is(range(-10,10)));
    }

    @Test
    public void testEquals() {
        assertThat(columnShape(1, 3),is(columnShape(1,3)));
        assertThat(columnShape(1, 3),is(not(columnShape(1, 4))));
        assertThat(columnShape(1, 3),is(not(columnShape(2, 3))));
        assertThat(columnShape(1, 3),is(not(columnShape(2, 4))));
    }

    @Test
    public void testHashCode() {
        assertThat(columnShape(1, 3).hashCode(), is(columnShape(1, 3).hashCode()));
        assertThat(columnShape(1, 3).hashCode(), is(not(columnShape(1, 4).hashCode())));
        assertThat(columnShape(1, 3).hashCode(), is(not(columnShape(2, 3).hashCode())));
        assertThat(columnShape(1, 3).hashCode(), is(not(columnShape(2, 4).hashCode())));
    }

    @Test
    public void testToString() {
        assertThat(columnShape(1, 3).toString(),is("(1,3)"));
    }

    private Range range(int minInclusive, int maxInclusive) {
        return new Range(minInclusive, maxInclusive);
    }

    private ColumnShape columnShape(int minInclusive, int maxInclusive) {
        return new ColumnShape(range(minInclusive, maxInclusive));
    }
}
