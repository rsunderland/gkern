package org.rimasu.gkern.op;

import java.util.Optional;

/**
 * How a derive book operation can fail.
 */
class MoveFailure {

    private final MoveFailureType tableFailure;
    private final Optional<Integer> tableNum;

    public MoveFailure(MoveFailureType tableFailure, Optional<Integer> tableNum) {
        this.tableFailure = tableFailure;
        this.tableNum = tableNum;
    }

    public MoveFailureType getTableFailure() {
        return tableFailure;
    }

    public Optional<Integer> getTableNum() {
        return tableNum;
    }
}
