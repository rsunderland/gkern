package org.rimasu.gkern.op;

import org.rimasu.gkern.grid.api.*;
import org.rimasu.gkern.grid.cursors.OrderCheckingCursor;
import org.rimasu.gkern.grid.cursors.SymDifferenceCursor;
import org.rimasu.gkern.grid.cursors.UnionCursor;
import org.rimasu.gkern.op.api.Result;

import java.util.Optional;

import static org.rimasu.gkern.op.MoveFailureType.*;

class DeriveTableOperator {

    private final TableFactory tableFactory;

    public DeriveTableOperator(TableFactory tableFactory) {
        this.tableFactory = tableFactory;
    }

    public Result<Table, MoveFailureType> apply(Table original, Table removed, Table added) {

        Optional<MoveFailureType> preconditionFailure = preconditionCheck(original, removed, added);

        if (preconditionFailure.isPresent()) {
            return failure(preconditionFailure.get());
        } else {
            return deriveTable(original, removed, added);
        }
    }

    private Optional<MoveFailureType> preconditionCheck(Table original, Table removed, Table added) {

        int numColumns = original.getNumColumns();

        if (removed.getNumColumns() < numColumns) {
            return Optional.of(REMOVED_TABLE_HAD_TOO_FEW_COLUMNS);
        }

        if (removed.getNumColumns() > numColumns) {
            return Optional.of(REMOVED_TABLE_HAD_TOO_MANY_COLUMNS);
        }

        if (added.getNumColumns() < numColumns) {
            return Optional.of(ADDED_TABLE_HAD_TOO_FEW_COLUMNS);
        }

        if (added.getNumColumns() > numColumns) {
            return Optional.of(ADDED_TABLE_HAD_TOO_MANY_COLUMNS);
        }

        return Optional.empty();
    }

    private Result<Table, MoveFailureType> deriveTable(Table original, Table removed, Table added) {
        int numColumns = original.getNumColumns();

        RowCursor originalCursor = original.createCursor();

        OrderCheckingCursor removedCursor = new OrderCheckingCursor(numColumns, removed.createCursor());
        OrderCheckingCursor addedCursor = new OrderCheckingCursor(numColumns, added.createCursor());
        SymDifferenceCursor survivors = new SymDifferenceCursor(originalCursor, removedCursor);
        UnionCursor combined = new UnionCursor(survivors, addedCursor);

        int maxRows = original.getNumRows() + added.getNumRows();

        Table result = gatherCursorIntoTable(numColumns, combined, maxRows);

        if (!removedCursor.isFullyOrdered()) {
            return failure(REMOVED_ROWS_NOT_FULLY_ORDERED);
        }

        if (!addedCursor.isFullyOrdered()) {
            return failure(ADDED_ROWS_NOT_FULLY_ORDERED);
        }

        if (survivors.getIgnoredCount() > 0) {
            return failure(REMOVED_ROW_NOT_FOUND_IN_ORIGINAL_TABLE);
        }

        if (combined.getIntersectionCount() > 0) {
            return failure(ADDED_ROW_ALREADY_IN_ORIGINAL_TABLE);
        }

        return Result.ok(result, MoveFailureType.class);
    }

    private Table gatherCursorIntoTable(int numColumns, ForwardRowCursor src, int maxRows) {

        Table result = tableFactory.createTable(numColumns, maxRows);

        RowCursor dest = result.createCursor();
        int rowsTransferred = 0;
        while (src.moveToNextRow()) {
            if (dest.moveToNextRow()) {
                dest.copyValues(src);
                rowsTransferred++;
            } else {
                throw new KernelException(KernelErrorCode.RESULT_TABLE_TOO_SMALL);
            }
        }

        result.truncateTo(rowsTransferred);
        return result;
    }


    private Result<Table, MoveFailureType> failure(MoveFailureType reason) {
        return Result.failure(Table.class, reason);
    }

}
