package org.rimasu.gkern.op;

import org.rimasu.gkern.grid.api.ArrayBook;
import org.rimasu.gkern.grid.api.Book;
import org.rimasu.gkern.grid.api.RowCursor;
import org.rimasu.gkern.grid.api.Table;
import org.rimasu.gkern.grid.array.IntArrayTable;
import org.rimasu.gkern.grid.buf.BufferFailure;
import org.rimasu.gkern.grid.buf.BufferTableFactory;
import org.rimasu.gkern.grid.buf.Frame;
import org.rimasu.gkern.op.api.Result;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;

class PackBookOperator {

    private final BufferTableFactory tableFactory;

    public PackBookOperator() {
        this(new BufferTableFactory());
    }

    private PackBookOperator(BufferTableFactory tableFactory) {
        this.tableFactory = tableFactory;
    }

    /**
     * Pack a book of tables into a byte buffer.
     * @param book book of tables to pack.
     * @return byte buffer containing table data.
     */
    public ByteBuffer pack(Book book) {
        Frame frame = packBook(book);
        return frame.getBackingBuffer();
    }

    public Result<Book, BufferFailure> unpack(ByteBuffer buffer) {
        Frame frame = new Frame(buffer);
        Result<Table, BufferFailure> summaryTableResult = tableFactory.bindPopulatedBuffer(1, frame.getBucket(1));
        if(summaryTableResult.isOk()) {
            return unpackFrame(summaryTableResult.get(), frame);
        } else {
            return Result.failure(Book.class, summaryTableResult.getFailureDetails());
        }
    }

    private Result<Book, BufferFailure> unpackFrame(Table summaryTable, Frame frame) {
        Collection<Table> tables = new ArrayList<>();
        RowCursor summaryRow = summaryTable.createCursor();
        int bucketNum = 2;
        while(summaryRow.moveToNextRow()) {
            int numColumns = summaryRow.getValue(1);
            ByteBuffer bucket = frame.getBucket(bucketNum++);
            Result<Table, BufferFailure> result = tableFactory.bindPopulatedBuffer(numColumns, bucket);
            if(result.isOk()) {
                tables.add(result.get());
            } else {
                return Result.failure(Book.class, result.getFailureDetails());
            }
        }
        return Result.ok(new ArrayBook(tables), BufferFailure.class);
    }

    private Frame packBook(Book book) {
        Table outlineTable = buildOutlineTable(book);
        return packTables(outlineTable,book);
    }

    private Frame packTables(Table outlineTable, Book book) {
        Frame frame = buildFrame(book, outlineTable);

        packTable(outlineTable, frame.getBucket(1));
        int i=2;
        for(Table table : book) {
            packTable(table, frame.getBucket(i++));
        }
        return frame;
    }

    private void packTable(Table outlineTable, ByteBuffer targetBuffer) {

        Table targetTable = tableFactory.formatAndBindEmptyBuffer(
                outlineTable.getNumColumns(),
                outlineTable.getNumRows(),
                outlineTable.getValueRange(),
                targetBuffer);

        RowCursor src = outlineTable.createCursor();
        RowCursor dest = targetTable.createCursor();

        while(src.moveToNextRow() && dest.moveToNextRow()) {
            dest.copyValues(src);
        }
    }

    private Frame buildFrame(Book book, Table outlineTable) {
        int[] bucketSizes = new int[book.getNumTables() + 1];
        bucketSizes[0] = calculateRequiredBufferSize(outlineTable);
        int i=1;
        for(Table table : book) {
            bucketSizes[i++] = calculateRequiredBufferSize(table);
        }
        return new Frame(bucketSizes);
    }

    int calculateRequiredBufferSize(Table existing) {
        return tableFactory.calculateRequiredBufferSize(existing.getNumColumns(), existing.getNumRows(), existing.getValueRange());
    }

    private Table buildOutlineTable(Book book) {
        Table outlineTable = new IntArrayTable(1, book.getNumTables());
        RowCursor outlineRow = outlineTable.createCursor();
        for(Table table : book) {
            outlineRow.moveToNextRow();
            outlineRow.setValue(1, table.getNumColumns());
        }
        return outlineTable;
    }
}
