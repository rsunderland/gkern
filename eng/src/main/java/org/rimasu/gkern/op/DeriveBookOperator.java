package org.rimasu.gkern.op;


import org.rimasu.gkern.grid.api.ArrayBook;
import org.rimasu.gkern.grid.api.Book;
import org.rimasu.gkern.grid.api.Table;
import org.rimasu.gkern.grid.api.TableFactory;
import org.rimasu.gkern.op.api.Result;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import static org.rimasu.gkern.op.MoveFailureType.*;


class DeriveBookOperator {

    private final DeriveTableOperator deriveTableOperator;

    public DeriveBookOperator(TableFactory tableFactory) {
        this.deriveTableOperator = new DeriveTableOperator(tableFactory);
    }


    public Result<Book, MoveFailure> apply(Book original, Book removed, Book added) {

        Optional<MoveFailureType> preconditionFailure = preconditionCheck(original, removed, added);

        if (preconditionFailure.isPresent()) {
            return failure(preconditionFailure.get());
        } else {
            return deriveBook(original, removed, added);
        }
    }

    private Optional<MoveFailureType> preconditionCheck(Book original, Book removed, Book added) {
        int numTables = original.getNumTables();

        if (removed.getNumTables() < numTables) {
            return Optional.of(REMOVED_BOOK_HAS_TOO_FEW_TABLES);
        }

        if (removed.getNumTables() > numTables) {
            return Optional.of(REMOVED_BOOK_HAS_TOO_MANY_TABLES);
        }

        if (added.getNumTables() < numTables) {
            return Optional.of(ADDED_BOOK_HAS_TOO_FEW_TABLES);
        }

        if (added.getNumTables() > numTables) {
            return Optional.of(ADDED_BOOK_HAS_TOO_MANY_TABLES);
        }

        return Optional.empty();
    }

    private Result<Book, MoveFailure> deriveBook(Book originalBook, Book removedBook, Book addedBook) {

        Collection<Table> tables  = new ArrayList<>();
        Iterator<Table> addedTables = addedBook.iterator();
        Iterator<Table> removedTables = removedBook.iterator();

        int i=1;
        for(Table originalTable: originalBook) {

            Result<Table, MoveFailureType> tableResult = deriveTableOperator.apply(
                    originalTable,
                    removedTables.next(),
                    addedTables.next());

            if(tableResult.isOk()) {
                tables.add(tableResult.get());
            } else {
                return failure(tableResult.getFailureDetails(),i);
            }

            i++;
        }

        return Result.ok(new ArrayBook(tables), MoveFailure.class);
    }

    private Result<Book, MoveFailure> failure(MoveFailureType type) {
        return Result.failure(Book.class, new MoveFailure(type, Optional.empty()));
    }

    private Result<Book, MoveFailure> failure(MoveFailureType type, int tableNum) {
        return Result.failure(Book.class, new MoveFailure(type, Optional.of(tableNum)));
    }

}
