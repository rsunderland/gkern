package org.rimasu.gkern.op.api;


import org.rimasu.gkern.grid.api.KernelException;

import static org.rimasu.gkern.grid.api.KernelErrorCode.FAILURE_RESULT_HAS_NO_VALUE;
import static org.rimasu.gkern.grid.api.KernelErrorCode.OK_RESULT_HAS_NO_FAILURE_DETAILS;

/**
 * Result of an operation that may fail that either contains the successful operation's
 * result or the unsuccessful operation's failure details.
 *
 * @param <S> result of the operation if it was successful.
 * @param <F> details of the operations failure if it was unsuccessful.
 */
public abstract class Result<S, F> {

    public static <S,F> Result<S, F> ok(S value, Class<F> failureType) {
        return new OkResult<>(value);
    }

    public static <S, F> Result<S, F> failure(Class<S> valueType, F failureDetails) {
        return new FailureResult<>(failureDetails);
    }

    public abstract boolean isOk();

    public abstract boolean isFailure();

    public abstract S get();

    public abstract F getFailureDetails();

    private static final class OkResult<S,F> extends Result<S,F> {

        private final S value;

        public OkResult(S value) {
            this.value = value;
        }

        @Override
        public boolean isOk() {
            return true;
        }

        @Override
        public boolean isFailure() {
            return false;
        }

        @Override
        public S get() {
            return value;
        }

        @Override
        public F getFailureDetails() {
            throw new KernelException(OK_RESULT_HAS_NO_FAILURE_DETAILS);
        }
    }

    private static final class FailureResult<S,F> extends Result<S,F>{

        private final F failureDetails;

        public FailureResult(F failureDetails) {
            this.failureDetails = failureDetails;
        }

        @Override
        public boolean isOk() {
            return false;
        }

        @Override
        public boolean isFailure() {
            return true;
        }

        @Override
        public S get() {
            throw new KernelException(FAILURE_RESULT_HAS_NO_VALUE);
        }

        @Override
        public F getFailureDetails() {
            return failureDetails;
        }
    }
}
