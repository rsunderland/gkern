package org.rimasu.gkern.grid.array;

import org.rimasu.gkern.grid.api.Range;
import org.rimasu.gkern.grid.api.RowCursor;
import org.rimasu.gkern.grid.api.RowView;
import org.rimasu.gkern.grid.api.Table;


public class EmptyTable implements Table {
    private int numColumns;

    public EmptyTable(int numColumns) {
        this.numColumns = numColumns;
    }

    @Override
    public int getNumColumns() {
        return numColumns;
    }

    @Override
    public int getNumRows() {
        return 0;
    }

    @Override
    public RowCursor createCursor() {
        return new EmptyCursor();
    }

    @Override
    public Range getValueRange() {
        return new Range(0,0);
    }

    @Override
    public void truncateTo(int numRows) {
        throw new UnsupportedOperationException();
    }

    private static class EmptyCursor implements RowCursor {

        @Override
        public int getRowNumber() {
            return 0;
        }

        @Override
        public boolean moveToPrevRow() {
            return false;
        }

        @Override
        public RowCursor moveToRow(int rowNumber) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean moveToNextRow() {
            return false;
        }

        @Override
        public int getValue(int columnNumber) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean isClear(int columnNumber) {
            throw new UnsupportedOperationException();
        }

        @Override
        public int compareValuesTo(RowView other) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setValue(int columnNumber, int value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void copyValues(RowView src) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void clearValue(int columnNumber) {
            throw new UnsupportedOperationException();
        }
    }
}
