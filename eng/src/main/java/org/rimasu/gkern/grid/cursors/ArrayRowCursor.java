package org.rimasu.gkern.grid.cursors;

import org.rimasu.gkern.grid.api.RowCursor;
import org.rimasu.gkern.grid.api.RowView;

/**
 * Adapter that supports presenting a one dimensional list of values as
 * a RowCursor.
 */
public abstract class ArrayRowCursor implements RowCursor {

    private final int numRows;
    private final int numColumns;

    private int rowNum;
    private int rowOffset;

    public ArrayRowCursor(int numRows, int numColumns) {
        this.numRows = numRows;
        this.numColumns =numColumns;
        moveToRow(0);
    }

    @Override
    public int getRowNumber() {
        return rowNum;
    }

    @Override
    public boolean moveToNextRow() {
        if(rowNum <= numRows) {
            rowNum++;
            rowOffset += numColumns;
            return rowNum <= numRows;
        } else {
            return false;
        }
    }

    @Override
    public boolean moveToPrevRow() {
        if(rowNum > 0) {
            rowNum--;
            rowOffset -= numColumns;
            return rowNum > 0;
        } else {
            return false;
        }
    }

    @Override
    public RowCursor moveToRow(int rowNumber) {
        checkRowNumber(rowNumber);
        this.rowNum = rowNumber;
        rowOffset = numColumns * (rowNumber - 1) -1;
        return this;
    }


    @Override
    public void setValue(int columnNumber, int value) {
        checkColumnNumber(columnNumber);
        setOffsetValue(rowOffset + columnNumber, value);
    }

    protected abstract void setOffsetValue(int offset, int value);

    @Override
    public void copyValues(RowView src) {
        for(int i=1;i<=numColumns;i++) {
            if(src.isClear(i)) {
                clearOffsetValue(rowOffset + i);
            } else {
                setOffsetValue(rowOffset + i, src.getValue(i));
            }
        }
    }

    @Override
    public int getValue(int columnNumber) {
        checkColumnNumber(columnNumber);
        return getOffsetValue(rowOffset + columnNumber);
    }

    protected abstract int getOffsetValue(int offset);

    @Override
    public void clearValue(int columnNumber) {
        checkColumnNumber(columnNumber);
        clearOffsetValue(rowOffset + columnNumber);
    }

    protected abstract void clearOffsetValue(int offset);

    @Override
    public boolean isClear(int columnNumber) {
        checkColumnNumber(columnNumber);
        return isOffsetClear(rowOffset + columnNumber);
    }

    protected abstract boolean isOffsetClear(int offset);

    private void checkRowNumber(int rowNumber) {
        if(rowNumber < 0 || rowNumber > numRows + 1)
            throw new ArrayIndexOutOfBoundsException();
    }

    private void checkColumnNumber(int columnNumber) {
        if(columnNumber < 1 || columnNumber > numColumns)
            throw new ArrayIndexOutOfBoundsException();
    }

    @Override
    public int compareValuesTo(RowView other) {
        for(int i=1;i<=numColumns;i++) {
            boolean thisClear = isClear(i);
            boolean thatClear = other.isClear(i);
            if(!thisClear && !thatClear) {
                int cmp = getOffsetValue(rowOffset + i) - other.getValue(i);
                if(cmp != 0)
                    return cmp;
            } else if(!thisClear) {
                return 1;
            } else if(!thatClear) {
                return -1;
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        StringBuilder bld = new StringBuilder();
        if(rowNum > 0 && rowNum <= numRows) {
            for(int i=1;i<=numColumns;i++) {
                if(isOffsetClear(rowOffset + i)) {
                    bld.append('?');
                } else {
                    bld.append(getOffsetValue(rowOffset + i));
                }
                bld.append(' ');
            }
        }
        bld.append("@");
        bld.append(rowNum);
        return bld.toString();
    }
}
