package org.rimasu.gkern.grid.api;


public class Range {

    private final int minInclusive;
    private final int maxInclusive;

    public Range(int minInclusive, int maxInclusive) {
        this.minInclusive = minInclusive;
        this.maxInclusive = maxInclusive;
    }

    public int getMinInclusive() {
        return minInclusive;
    }

    public int getMaxInclusive() {
        return maxInclusive;
    }

    public boolean contains(int candidate) {
        return candidate >= minInclusive && candidate <= maxInclusive;
    }

    public boolean contains(Range other) {
        return contains(other.minInclusive) && contains(other.maxInclusive);
    }

    @Override
    public String toString() {
        return "(" + minInclusive + ',' + maxInclusive + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Range range = (Range) o;

        return maxInclusive == range.maxInclusive && minInclusive == range.minInclusive;

    }

    @Override
    public int hashCode() {
        int result = minInclusive;
        result = 31 * result + maxInclusive;
        return result;
    }

    public Range unionWith(Range other) {
        int minMin = Math.min(minInclusive, other.minInclusive);
        int maxMax = Math.max(maxInclusive, other.maxInclusive);
        return new Range(minMin, maxMax);
    }

    public Range unionWith(int other) {
        int minMin = Math.min(minInclusive, other);
        int maxMax = Math.max(maxInclusive, other);
        return new Range(minMin, maxMax);
    }
}
