package org.rimasu.gkern.grid.array;

import org.rimasu.gkern.grid.api.*;

/**
 * Table factory that creates tables backed by arrays of integers.
 */
public class IntArrayTableFactory implements TableFactory {

    @Override
    public Table createTable(int numColumns, int numRows) {
        return new IntArrayTable(numColumns, numRows);
    }
}
