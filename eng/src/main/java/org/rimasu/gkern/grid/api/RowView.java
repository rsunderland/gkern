package org.rimasu.gkern.grid.api;

/**
 * Provides a view of a row in a table. The view
 * may change which row it is pointing to; it is
 * provides transient access to row, not the
 * storage for the row.
 */
public interface RowView {

    /**
     * Get the value of a cell in the row that the cursor is pointing to.
     * @param columnNumber the column number of the value to get.
     * @return value of cell.
     * @throws java.util.NoSuchElementException if cell is clear
     * @throws ArrayIndexOutOfBoundsException if column number is not valid or cursor is not on a row.
     */
    int getValue(int columnNumber);

    /**
     * Test whether a cell in the row that this cursor is pointing to is clear
     * @param columnNumber the number of the column to test.
     * @return true if column is clear, otherwise false.
     * @throws ArrayIndexOutOfBoundsException if column number is not valid or cursor is not on a row.
     */
    boolean isClear(int columnNumber);

    /**
     * Compares the values currently in this view to the values currently in the other row.
     * @param other row to compare to.
     * @return negative if this row is lexically before the other row, positive if this
     * row is lexically after the other other, otherwise zero.
     */
    int compareValuesTo(RowView other);

    /**
     * Set the value of a cell in the row that the cursor is pointing to.
     * @param columnNumber the column number of the value to set.
     * @param value the new value for the cell.
     * @throws UnsupportedOperationException if this is a read only view.
     */
    void setValue(int columnNumber, int value);

    /**
     * Set the values of this row to match those in the source.
     * @param src row to copy from
     * @throws UnsupportedOperationException if this is a read only view.
     * @throws ArrayIndexOutOfBoundsException if src row is narrower than this row.
     */
    void copyValues(RowView src);

    /**
     * Clear the value of a cell in the row that this cursor is pointing to.
     * @param columnNumber the column number of the value to set.
     * @throws ArrayIndexOutOfBoundsException if column number is not valid or cursor is not on a row.
     * @throws UnsupportedOperationException if this is a read only view.
     */
    void clearValue(int columnNumber);
}
