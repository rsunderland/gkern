package org.rimasu.gkern.grid.buf;

import org.rimasu.gkern.grid.api.*;

import java.nio.ByteBuffer;

class ByteBufferTableFactory extends InnerBufferTableFactory {

    private static final int CELL_SIZE = 1;
    private static final Range MAX_VALUE_RANGE = new Range(Byte.MIN_VALUE + 1, Byte.MAX_VALUE);
    private static final byte SIGNATURE_BYTE = 0x01;

    public ByteBufferTableFactory() {
        super(SIGNATURE_BYTE, CELL_SIZE, MAX_VALUE_RANGE);
    }

    @Override
    protected Table buildTable(int numColumns, int numRows, ByteBuffer source) {
        source.position(NUM_HEADER_BYTES);
        ByteBuffer dataView = source.slice();
        return new ByteBufferTable(numColumns, numRows, dataView);
    }

}
