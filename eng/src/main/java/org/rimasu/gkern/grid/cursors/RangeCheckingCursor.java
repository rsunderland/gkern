package org.rimasu.gkern.grid.cursors;


import org.rimasu.gkern.grid.api.*;

public class RangeCheckingCursor extends ReadOnlyRowView implements ForwardRowCursor {

    private final ForwardRowCursor delegate;
    private final TableShape shape;
    private final int numColumns;
    private boolean inRange;

    public RangeCheckingCursor(TableShape shape, ForwardRowCursor delegate) {
        this.delegate = delegate;
        this.shape = shape;
        this.inRange = true;
        this.numColumns = shape.getNumColumns();
    }

    @Override
    public boolean moveToNextRow() {
        if(inRange && delegate.moveToNextRow()) {
            for(int i=1;i<=numColumns;i++) {
                if(isClear(i) || !shape.getColumnShape(i).getRange().contains(getValue(i))) {
                    inRange = false;
                    break;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getValue(int columnNumber) {
        return delegate.getValue(columnNumber);
    }

    @Override
    public boolean isClear(int columnNumber) {
        return delegate.isClear(columnNumber);
    }

    @Override
    public int compareValuesTo(RowView other) {
        return delegate.compareValuesTo(other);
    }

    public boolean isInRange() {
        return inRange;
    }
}
