package org.rimasu.gkern.grid.api;

/**
 * Base class for RowViews that do not support modification.
 */
public abstract class ReadOnlyRowView implements RowView {

    @Override
    public void setValue(int columnNumber, int value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void copyValues(RowView src) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clearValue(int columnNumber) {
        throw new UnsupportedOperationException();
    }
}
