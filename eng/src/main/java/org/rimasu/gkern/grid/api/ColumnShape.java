package org.rimasu.gkern.grid.api;


public class ColumnShape {

    private final Range range;

    public ColumnShape(Range range) {
        this.range = range;
    }

    public Range getRange() {
        return range;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ColumnShape that = (ColumnShape) o;

        return range.equals(that.range);
    }

    @Override
    public int hashCode() {
        return range.hashCode();
    }

    @Override
    public String toString() {
        return range.toString();
    }

}
