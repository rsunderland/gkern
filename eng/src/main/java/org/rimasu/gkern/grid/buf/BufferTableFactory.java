package org.rimasu.gkern.grid.buf;

import org.rimasu.gkern.grid.api.*;
import org.rimasu.gkern.grid.array.EmptyTable;
import org.rimasu.gkern.op.api.Result;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

import static org.rimasu.gkern.grid.buf.BufferFailure.TABLE_BUFFER_SIGNATURE_BYTE_NOT_RECOGNISED;


public class BufferTableFactory {

    private final List<InnerBufferTableFactory> tableFactories;

    public BufferTableFactory() {
        this(Arrays.asList(
                new ByteBufferTableFactory(),
                new ShortBufferTableFactory(),
                new IntBufferTableFactory()
        ));
    }

    private BufferTableFactory(List<InnerBufferTableFactory> tableFactories) {
        this.tableFactories = tableFactories;
    }


    /**
     * Build a table from a populated buffer. Reads the table size from
     * the data and binds the remaining buffer as data (assuming it
     * is the correct size).
     *
     * @param numColumns number of columns in the table.
     * @param buffer     buffer containing header and data
     * @return data bound into a table if table size is consistent with size of buffer, otherwise a failure.
     */
    public Result<Table, BufferFailure> bindPopulatedBuffer(int numColumns, ByteBuffer buffer) {
        if(buffer.hasRemaining()) {
            InnerBufferTableFactory factory = pickFactory(buffer);
            if(factory == null) {
                return Result.failure(Table.class, TABLE_BUFFER_SIGNATURE_BYTE_NOT_RECOGNISED);
            } else {
                return factory.bindPopulatedBuffer(numColumns, buffer);
            }
        } else {
            return Result.ok(new EmptyTable(numColumns), BufferFailure.class);
        }
    }

    /**
     * Calculate the size of buffer required to store a table of specified size.
     * Equivalent to calling calculateRequiredBufferSize(size, getMaxValueRange());
     *
     * @param numColumns number of columns in table.
     * @param numRows number of rows in table.
     * @param valueRange  the range of value that table needs to support.
     * @return number of bytes required to store data.
     */
    public int calculateRequiredBufferSize(int numColumns, int numRows, Range valueRange) {
        InnerBufferTableFactory factory = pickFactory(valueRange);
        return factory.calculateRequiredBufferSize(numColumns, numRows);
    }


    /**
     * Format a empty buffer to include table size and bind the remaining buffer as data (assuming
     * it is the correct size). This method must succeed if the empty buffer has a size that matches <code>calculateRequiredBufferSize()</code>.
     *
     * @param numColumns number of columns in table.
     * @param numRows number of rows in table.
     * @param valueRange  the range of value that table needs to support.
     * @param emptyBuffer buffer to format.
     * @return emptyBuffer bound into a table if table size is consistent with size of buffer, otherwise failure.
     */
    public Table formatAndBindEmptyBuffer(int numColumns, int numRows, Range valueRange, ByteBuffer emptyBuffer) {
        InnerBufferTableFactory factory = pickFactory(valueRange);
        return factory.formatAndBindEmptyBuffer(numColumns, numRows, emptyBuffer);
    }

    private InnerBufferTableFactory pickFactory(Range valueRange) {
        for (InnerBufferTableFactory candidate : tableFactories) {
            if (candidate.getMaxValueRange().contains(valueRange)) {
                return candidate;
            }
        }
        throw new KernelException(KernelErrorCode.NO_BUFFER_FACTORY_FOUND_FOR_VALUE_RANGE);
    }

    private InnerBufferTableFactory pickFactory(ByteBuffer buffer) {
        for (InnerBufferTableFactory candidate : tableFactories) {
            if (candidate.matchesBuffer(buffer)) {
                return candidate;
            }
        }
        return null;
    }

}
