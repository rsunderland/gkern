package org.rimasu.gkern.grid.api;

import org.rimasu.gkern.grid.api.RowView;

/**
 * Cursor that can step through rows in one direction (forward).
 */
public interface ForwardRowCursor extends RowView {

    /**
     * Move cursor to next row.
     * @return true if move left cursor on row with data, otherwise false.
     */
    boolean moveToNextRow();

}
