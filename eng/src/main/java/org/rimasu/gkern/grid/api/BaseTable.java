package org.rimasu.gkern.grid.api;


import static org.rimasu.gkern.grid.api.KernelErrorCode.*;

public abstract class BaseTable implements Table {

    protected final int numColumns;
    private int numRows;

    protected BaseTable(int numColumns, int numRows) {
        this.numColumns = numColumns;
        this.numRows = numRows;
    }

    @Override
    public int getNumColumns() {
        return numColumns;
    }

    @Override
    public int getNumRows() {
        return numRows;
    }

    @Override
    public Range getValueRange() {
        RowCursor cursor = createCursor();
        Range range = null;
        while(cursor.moveToNextRow()) {
            for(int i=1;i<=numColumns;i++) {
                if (!cursor.isClear(i)) {
                    int value = cursor.getValue(i);
                    if(range == null) {
                        range = new Range(value, value);
                    } else {
                        range = range.unionWith(value);
                    }
                }
            }
        }
        if(range == null) {
            return new Range(0,0);
        }
        return range;
    }

    @Override
    public void truncateTo(int numRows) {
        if(numRows > this.numRows) {
            throw new KernelException(TABLE_CAN_NOT_BE_EXTENDED);
        } else if (numRows < 0) {
            throw new KernelException(TABLE_HAS_NEGATIVE_NUMBER_OF_ROWS);
        } else {
            this.numRows = numRows;
        }
    }

    public String toString() {
        StringBuilder bld = new StringBuilder();
        RowCursor c = createCursor();
        while(c.moveToNextRow())
        {
            for(int i=1;i<=numColumns;i++) {
                if(c.isClear(i)) {
                    bld.append('?');
                }else{
                    bld.append(c.getValue(i));
                }
                bld.append(' ');
            }
            bld.append('\n');
        }
        return bld.toString();
    }
}
