package org.rimasu.gkern.grid.api;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayBook implements Book {

    private final Table[] tables;

    public ArrayBook(Collection<Table> tables) {
        this(tables.toArray(new Table[tables.size()]));
    }

    private ArrayBook(Table[] tables) {
        this.tables = tables;
    }

    public int getNumTables() {
        return tables.length;
    }

    public String toString() {
        StringBuilder bld = new StringBuilder();
        for(Table table : tables) {
            bld.append(table);
            bld.append('\n');
        }
        return bld.toString();
    }

    @Override
    public Iterator<Table> iterator() {
        return new ArrayBookIterator();
    }

    private class ArrayBookIterator implements Iterator<Table> {

        private int index;

        public ArrayBookIterator() {
            this.index = 0;
        }

        @Override
        public boolean hasNext() {
            return index < tables.length;
        }

        @Override
        public Table next() {
            if(hasNext()) {
                return tables[index++];
            }
            throw new NoSuchElementException();
        }
    }
}
