package org.rimasu.gkern.grid.api;

/**
 * Interface for creating new books based on a book shape and a profile.
 */
public interface TableFactory {

    /**
     * Create a new table with supplied shape and number of rows.
     * @param numColumns number of columns in the table.
     * @param numRows number of rows in the table.
     * @return table matching shape and and numRows
     */
    public Table createTable(int numColumns, int numRows);
}
