package org.rimasu.gkern.grid.buf;

import org.rimasu.gkern.grid.api.*;
import org.rimasu.gkern.op.api.Result;

import java.nio.ByteBuffer;
import java.util.Optional;

import static org.rimasu.gkern.grid.buf.BufferFailure.TABLE_BUFFER_TOO_LARGE;
import static org.rimasu.gkern.grid.buf.BufferFailure.TABLE_BUFFER_TOO_SMALL;

abstract class InnerBufferTableFactory {

    static final byte SIGNATURE_BYTE_OFFSET = 0;
    private static final int NUM_ROWS_HEADER_OFFSET = 1;
    static final int NUM_HEADER_BYTES = 3;

    private final byte signatureByte;
    private final int cellSize;
    private final Range maxValueRange;

    InnerBufferTableFactory(byte signatureByte, int cellSize, Range maxValueRange) {
        this.signatureByte = signatureByte;
        this.cellSize = cellSize;
        this.maxValueRange = maxValueRange;
    }

    /**
     *
     * @param numColumns number of columns in table.
     * @param numRows number of rows in table.
     * @return number of bytes required to store table.
     */
    public int calculateRequiredBufferSize(int numColumns, int numRows) {
        int dataBytes = numColumns * numRows * cellSize;
        return NUM_HEADER_BYTES + dataBytes;
    }

    public Range getMaxValueRange() {
        return maxValueRange;
    }

    public boolean matchesBuffer(ByteBuffer buffer) {
        return buffer.get(SIGNATURE_BYTE_OFFSET) == signatureByte;
    }

    public Result<Table, BufferFailure> bindPopulatedBuffer(int numColumns, ByteBuffer buffer) {
        ByteBuffer source = buffer.slice();

        if (source.remaining() < NUM_HEADER_BYTES) {
            return Result.failure(Table.class, TABLE_BUFFER_TOO_SMALL);
        }

        int numRows = readHeader( source);

        Optional<BufferFailure> possibleFailure = checkBufferSize(numColumns, numRows, source);
        if (possibleFailure.isPresent()) {
            return Result.failure(Table.class, possibleFailure.get());
        }

        return Result.ok(buildTable(numColumns, numRows, source), BufferFailure.class);
    }

    public Table formatAndBindEmptyBuffer(int numColumns, int numRows, ByteBuffer buffer) {

        ByteBuffer source = buffer.slice();
        Optional<BufferFailure> possibleFailure = checkBufferSize(numColumns, numRows, source);
        if (possibleFailure.isPresent()) {
            throw new KernelException(KernelErrorCode.EMPTY_BUFFER_INCORRECT_SIZE);
        }
        writeHeader(numRows, source);
        return buildTable(numColumns, numRows, source);

    }

    protected abstract Table buildTable(int numColumns, int numRows, ByteBuffer source);

    private void writeHeader(int numRows, ByteBuffer source) {
        source.put(SIGNATURE_BYTE_OFFSET, signatureByte);
        source.putShort(NUM_ROWS_HEADER_OFFSET, (short) numRows);
    }

    private int  readHeader(ByteBuffer source) {
        byte actualSignature = source.get(SIGNATURE_BYTE_OFFSET);

        if (actualSignature != signatureByte) {
            throw new KernelException(KernelErrorCode.BUFFER_TABLE_HAS_BAD_SIGNATURE_BYTE);
        }

        return source.getShort(NUM_ROWS_HEADER_OFFSET);
    }

    private Optional<BufferFailure> checkBufferSize(int numColumns, int numRows, ByteBuffer source) {
        int totalBytes = source.remaining();
        int expectedBytes = calculateRequiredBufferSize(numColumns, numRows);

        if (totalBytes < expectedBytes) {
            return Optional.of(TABLE_BUFFER_TOO_SMALL);
        }

        if (totalBytes > expectedBytes) {
            return Optional.of(TABLE_BUFFER_TOO_LARGE);
        }

        return Optional.empty();
    }


}
