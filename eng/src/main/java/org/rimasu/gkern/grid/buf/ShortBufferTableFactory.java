package org.rimasu.gkern.grid.buf;

import org.rimasu.gkern.grid.api.*;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

class ShortBufferTableFactory extends InnerBufferTableFactory {

    private static final int CELL_SIZE = 2;
    private static final Range MAX_VALUE_RANGE = new Range(Short.MIN_VALUE + 1, Short.MAX_VALUE);
    private static final byte SIGNATURE_BYTE = 0x02;

    public ShortBufferTableFactory() {
        super(SIGNATURE_BYTE, CELL_SIZE, MAX_VALUE_RANGE);
    }

    @Override
    protected Table buildTable(int numColumns, int numRows, ByteBuffer source) {
        source.position(NUM_HEADER_BYTES);
        ShortBuffer dataView = source.slice().asShortBuffer();
        return new ShortBufferTable(numColumns, numRows, dataView);
    }

}
