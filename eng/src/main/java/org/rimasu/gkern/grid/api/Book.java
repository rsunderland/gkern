package org.rimasu.gkern.grid.api;

/**
 * Ordered collection of {@link org.rimasu.gkern.grid.api.Table}s.
 */
public interface Book extends Iterable<Table> {
    int getNumTables();
}
