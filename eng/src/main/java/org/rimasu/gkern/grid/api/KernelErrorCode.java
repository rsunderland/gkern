package org.rimasu.gkern.grid.api;

public enum KernelErrorCode {

    TABLE_CAN_NOT_BE_EXTENDED(1),
    TABLE_HAS_NEGATIVE_NUMBER_OF_ROWS(2),
    OK_RESULT_HAS_NO_FAILURE_DETAILS(3),
    FAILURE_RESULT_HAS_NO_VALUE(4),
    RESULT_TABLE_TOO_SMALL(5),
    VALUE_OVERFLOW(6),
    VALUE_UNDERFLOW(7),
    BUFFER_TABLE_HAS_BAD_SIGNATURE_BYTE(8),
    NO_BUFFER_FACTORY_FOUND_FOR_VALUE_RANGE(9),
    EMPTY_BUFFER_INCORRECT_SIZE(10);

    private int number;

    private KernelErrorCode(int number) {
        this.number = number;
    }

    public String getId() {
        return String.format("GKE%04d", number);
    }
}
