package org.rimasu.gkern.grid.api;


import java.util.Arrays;
import java.util.Collection;

public class TableShape {

    private final int maxRows;
    private final ColumnShape[] columnShapes;

    public TableShape(int maxRows, Collection<ColumnShape> columnShapes) {
        this(maxRows, columnShapes.toArray(new ColumnShape[columnShapes.size()]));
    }

    TableShape(int maxRows, ColumnShape[] columnShapes) {
        this.maxRows = maxRows;
        this.columnShapes = columnShapes;
    }

    public int getMaxRows() {
        return maxRows;
    }

    public int getNumColumns() {
        return columnShapes.length;
    }

    public ColumnShape getColumnShape(int columnNumber) {
        return columnShapes[columnNumber - 1];
    }

    @Override
    public String toString() {
        StringBuilder bld = new StringBuilder();
        bld.append('(');
        bld.append(maxRows);
        bld.append(",[");
        for(ColumnShape columnShape: columnShapes) {
            bld.append(columnShape);
        }
        bld.append("])");
        return bld.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TableShape that = (TableShape) o;

        if (maxRows != that.maxRows) return false;
        return Arrays.equals(columnShapes, that.columnShapes);

    }

    @Override
    public int hashCode() {
        int result = maxRows;
        result = 31 * result + Arrays.hashCode(columnShapes);
        return result;
    }
}
