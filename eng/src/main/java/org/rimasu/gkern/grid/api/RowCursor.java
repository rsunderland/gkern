package org.rimasu.gkern.grid.api;

/**
 * Provides access to the data in a {@link org.rimasu.gkern.grid.api.Table}.
 */
public interface RowCursor extends ForwardRowCursor {

    /**
     * Current row number. First row is row 1, newly constructed RowCursors are normally
     * on row 0 (i.e., before first row).
     * @return current row number.
     */
    int getRowNumber();

    /**
     * Move cursor to previous row.
     * @return true if move left cursor on row with data, otherwise false.
     */
    boolean moveToPrevRow();

    /**
     * Move cursor to specified row.
     * @param rowNumber row number to move to.
     * @throws java.util.NoSuchElementException if row number more than one row beyond table (it is possible
     * to move the cursor to before the first row, or after the last row).
     * @return self pointer for chaining.
     */
    RowCursor moveToRow(int rowNumber);

}
