package org.rimasu.gkern.grid.cursors;


import org.rimasu.gkern.grid.api.ForwardRowCursor;

/**
 * Dual cursor that shows that values that are in either the left cursor or right cursor.
 * Assumes that the rows in the left and right cursor are fully sorted (no duplicates).
 */
public class UnionCursor extends DualForwardCursor implements ForwardRowCursor {

    private int intersectionCount;

    public UnionCursor(ForwardRowCursor left, ForwardRowCursor right) {
        super(left, right);
    }

    @Override
    public boolean moveToNextRow() {
        step();
        if(onLeft() && onRight()) {
            intersectionCount++;
        }
        return onLeft() || onRight();
    }

    public int getIntersectionCount() {
        return intersectionCount;
    }
}
