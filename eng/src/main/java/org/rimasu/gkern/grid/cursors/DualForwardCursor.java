package org.rimasu.gkern.grid.cursors;


import org.rimasu.gkern.grid.api.ReadOnlyRowView;
import org.rimasu.gkern.grid.api.RowView;
import org.rimasu.gkern.grid.api.ForwardRowCursor;

public abstract class DualForwardCursor  extends ReadOnlyRowView implements ForwardRowCursor {

    private final ForwardRowCursor left;
    private final ForwardRowCursor right;
    private ForwardRowCursor selected;

    private boolean leftFinished;
    private boolean rightFinished;
    private boolean onLeft;
    private boolean onRight;

    DualForwardCursor(ForwardRowCursor left, ForwardRowCursor right) {
        this.left = left;
        this.right = right;
        this.leftFinished = !left.moveToNextRow();
        this.rightFinished = !right.moveToNextRow();
        this.onRight = false;
        this.onLeft = false;
        this.selected = null;
    }

    void step() {
        if(leftFinished) {
            if(!rightFinished) {
                moveRight();
            }
        } else {
            if(rightFinished) {
                moveLeft();
            } else {
                moveLeftRightOrBoth();
            }
        }

        if(onLeft) {
            selected = left;
        } else if(onRight) {
            selected = right;
        } else {
            selected = null;
        }
    }

    private void moveLeft() {
        if(onLeft) {
            leftFinished = !left.moveToNextRow();
        }
        onLeft = !leftFinished;
    }

    private void moveRight() {
        if(onRight) {
            rightFinished = !right.moveToNextRow();
        }
        onRight = !rightFinished;
    }

    private void moveLeftRightOrBoth() {
        if(onLeft) {
            leftFinished = !left.moveToNextRow();
        }

        if(onRight) {
            rightFinished = !right.moveToNextRow();
        }

        if(!leftFinished && !rightFinished) {
            int cmp = left.compareValuesTo(right);
            onLeft  = cmp <= 0;
            onRight = cmp >= 0;
        } else  {
            onLeft  = !leftFinished;
            onRight = !rightFinished;
        }
    }



    boolean onLeft() {
        return onLeft;
    }

    boolean onRight() {
        return onRight;
    }

    @Override
    public int getValue(int columnNumber) {
        return selected.getValue(columnNumber);
    }

    @Override
    public boolean isClear(int columnNumber) {
        return selected.isClear(columnNumber);
    }

    @Override
    public int compareValuesTo(RowView other) {
        return selected.compareValuesTo(other);
    }

    boolean isFinished() {
        return leftFinished && rightFinished;
    }


    @Override
    public String toString() {
        StringBuilder bld = new StringBuilder();
        bld.append('(');
        if(onLeft)
            bld.append('*');

        bld.append(left);
        bld.append(' ');
        if(onRight)
            bld.append('*');
        bld.append(right);

        bld.append(')');
        return bld.toString();
    }
}
