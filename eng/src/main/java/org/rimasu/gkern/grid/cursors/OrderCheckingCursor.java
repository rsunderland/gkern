package org.rimasu.gkern.grid.cursors;


import org.rimasu.gkern.grid.api.*;

public class OrderCheckingCursor extends ReadOnlyRowView implements ForwardRowCursor {

    private final ForwardRowCursor delegate;
    private boolean hasPrev;
    private RowBuffer buffer;
    private boolean fullyOrdered;

    public OrderCheckingCursor(int numColumns, ForwardRowCursor delegate) {
        this.delegate = delegate;
        this.buffer = new RowBuffer(numColumns);
        this.hasPrev = false;
        this.fullyOrdered = true;
    }

    @Override
    public boolean moveToNextRow() {
        if(fullyOrdered && delegate.moveToNextRow()) {
            if(hasPrev) {
                int cmp = buffer.compareValuesTo(delegate);
                if(cmp >= 0) {
                    fullyOrdered = false;
                } else {
                    buffer.copyValues(delegate);
                }
            }

            buffer.copyValues(delegate);
            hasPrev = true;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getValue(int columnNumber) {
        return buffer.getValue(columnNumber);
    }

    @Override
    public boolean isClear(int columnNumber) {
        return buffer.isClear(columnNumber);
    }

    @Override
    public int compareValuesTo(RowView other) {
        return buffer.compareValuesTo(other);
    }

    public boolean isFullyOrdered() {
        return fullyOrdered;
    }
}
