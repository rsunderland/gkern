package org.rimasu.gkern.grid.buf;

import org.rimasu.gkern.grid.api.BaseTable;
import org.rimasu.gkern.grid.api.RowCursor;
import org.rimasu.gkern.grid.cursors.ArrayRowCursor;

import java.nio.IntBuffer;

class IntBufferTable extends BaseTable {

    private final IntBuffer data;

    IntBufferTable(int numColumns, int numRows, IntBuffer buffer) {
        super(numColumns, numRows);
        this.data = buffer;
    }

    @Override
    public RowCursor createCursor() {
        return new IntBufferArrayRowCursor(getNumRows(), numColumns);
    }

    private class IntBufferArrayRowCursor extends ArrayRowCursor {

        public IntBufferArrayRowCursor(int numRows, int numColumns) {
            super(numRows, numColumns);
            moveToRow(0);
        }

        protected void setOffsetValue(int offset, int value) {
            data.put(offset, value);
        }

        protected int getOffsetValue(int offset) {
            return data.get(offset);
        }

        protected void clearOffsetValue(int offset) {
            data.put(offset, Integer.MIN_VALUE);
        }

        protected boolean isOffsetClear(int offset) {
            return data.get(offset) == Integer.MIN_VALUE;
        }
    }
}
