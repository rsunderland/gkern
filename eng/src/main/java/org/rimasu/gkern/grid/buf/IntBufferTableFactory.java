package org.rimasu.gkern.grid.buf;


import org.rimasu.gkern.grid.api.*;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

class IntBufferTableFactory extends InnerBufferTableFactory {

    private static final int CELL_SIZE = 4;
    private static final Range MAX_VALUE_RANGE = new Range(Integer.MIN_VALUE + 1, Integer.MAX_VALUE);
    private static final byte SIGNATURE_BYTE = 0x03;

    public IntBufferTableFactory() {
        super(SIGNATURE_BYTE, CELL_SIZE, MAX_VALUE_RANGE);
    }

    @Override
    protected Table buildTable(int numColumns, int numRows, ByteBuffer source) {
        source.position(NUM_HEADER_BYTES);
        IntBuffer dataView = source.slice().asIntBuffer();
        return new IntBufferTable(numColumns, numRows, dataView);
    }

}
