package org.rimasu.gkern.grid.api;


public class RowBuffer implements RowView {

    private final int numColumns;
    private final Integer[] values;

    public RowBuffer(int numColumns) {
        this.numColumns = numColumns;
        this.values = new Integer[numColumns];
    }

    @Override
    public int getValue(int columnNumber) {
        return values[columnNumber-1];
    }

    @Override
    public boolean isClear(int columnNumber) {
        return values[columnNumber-1] == null;
    }

    @Override
    public int compareValuesTo(RowView other) {
        for(int i=1;i<=numColumns;i++) {
            boolean thisClear = isClear(i);
            boolean thatClear = other.isClear(i);
            if(!thisClear && !thatClear) {
                int cmp = getValue(i) - other.getValue(i);
                if(cmp != 0)
                    return cmp;
            } else if(!thisClear) {
                return 1;
            } else if(!thatClear) {
                return -1;
            }
        }
        return 0;
    }


    @Override
    public void setValue(int columnNumber, int value) {
        values[columnNumber-1] = value;
    }

    @Override
    public void copyValues(RowView src) {
        for(int i=0;i<numColumns;i++) {
            if(src.isClear(i+1)) {
                values[i] = null;
            } else {
                values[i] = src.getValue(i+1);
            }
        }
    }

    @Override
    public void clearValue(int columnNumber) {
        values[columnNumber-1] = null;
    }

    @Override
    public String toString() {
        StringBuilder bld = new StringBuilder();

        for(int i=1;i<=numColumns;i++) {
            if(isClear(i)) {
                bld.append('?');
            } else {
                bld.append(getValue(i));
            }
            bld.append(' ');
        }

        return bld.toString();
    }
}
