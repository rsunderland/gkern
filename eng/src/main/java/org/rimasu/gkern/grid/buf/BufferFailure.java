package org.rimasu.gkern.grid.buf;


public enum BufferFailure {
    TABLE_BUFFER_TOO_SMALL, TABLE_BUFFER_TOO_LARGE, TABLE_BUFFER_SIGNATURE_BYTE_NOT_RECOGNISED,
}
