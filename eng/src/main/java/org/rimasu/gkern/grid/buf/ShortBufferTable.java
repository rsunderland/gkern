package org.rimasu.gkern.grid.buf;

import org.rimasu.gkern.grid.api.*;
import org.rimasu.gkern.grid.cursors.ArrayRowCursor;

import java.nio.ShortBuffer;

class ShortBufferTable extends BaseTable {

    private final ShortBuffer data;

    ShortBufferTable(int numColumns, int numRows, ShortBuffer buffer) {
        super(numColumns, numRows);
        this.data = buffer;
    }

    @Override
    public RowCursor createCursor() {
        return new ShortBufferArrayRowCursor(getNumRows(), numColumns);
    }


    private class ShortBufferArrayRowCursor extends ArrayRowCursor {

        public ShortBufferArrayRowCursor(int numRows, int numColumns) {
            super(numRows, numColumns);
            moveToRow(0);
        }

        protected void setOffsetValue(int offset, int intValue) {
            checkShort(intValue);
            data.put(offset, (short)intValue);
        }

        private void checkShort(int intValue) {
            if(intValue > Short.MAX_VALUE) {
                throw new KernelException(KernelErrorCode.VALUE_OVERFLOW);
            }
            if(intValue <= Short.MIN_VALUE) {
                throw new KernelException(KernelErrorCode.VALUE_UNDERFLOW);
            }
        }

        protected int getOffsetValue(int offset) {
            return data.get(offset);
        }

        protected void clearOffsetValue(int offset) {
            data.put(offset, Short.MIN_VALUE);
        }

        protected boolean isOffsetClear(int offset) {
            return data.get(offset) == Short.MIN_VALUE;
        }
    }
}
