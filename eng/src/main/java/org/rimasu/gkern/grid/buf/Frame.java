package org.rimasu.gkern.grid.buf;

import java.nio.ByteBuffer;

/**
* Created by rich on 22/09/15.
*/
public class Frame {

    private final ByteBuffer backingBuffer;
    private final int numBuckets;
    private final int[] bucketSizes;
    private final ByteBuffer[] buckets;

    public Frame(ByteBuffer source) {
        this.backingBuffer = source;
        numBuckets = source.getShort();
        bucketSizes = readBucketSizes(source);
        buckets = buildBuckets();
        backingBuffer.flip();
    }


    public Frame(int[] bucketSizes) {
        this.bucketSizes = bucketSizes.clone();
        this.numBuckets = bucketSizes.length;
        this.backingBuffer = ByteBuffer.allocate(calculateTotalSize());
        writeHeader();
        buckets = buildBuckets();
        backingBuffer.flip();
    }

    public ByteBuffer getBackingBuffer() {
        return backingBuffer.duplicate();
    }


    private void writeHeader() {
        backingBuffer.putShort((short)numBuckets);
        for(int i=0;i<numBuckets;i++) {
            backingBuffer.putShort((short)bucketSizes[i]);
        }
    }

    private int calculateTotalSize() {
        return calculateHeaderSize() + calculateBodySize();
    }

    private int calculateBodySize() {
        int total = 0;
        for(int bucketSize : bucketSizes) {
            total += bucketSize;
        }
        return total;
    }

    private int calculateHeaderSize() {
        return 2 + numBuckets * 2;
    }

    private ByteBuffer[] buildBuckets() {
        int i = 0;
        ByteBuffer[] result = new ByteBuffer[numBuckets];
        for(int bucketSize : bucketSizes) {
            ByteBuffer slice = backingBuffer.slice();
            slice.limit(bucketSize);
            result[i++] = slice;
            backingBuffer.position(backingBuffer.position() + bucketSize);
        }
        return result;
    }


    private int[] readBucketSizes(ByteBuffer source) {
        int[] bucketSizes = new int[numBuckets];
        for(int i=0;i<numBuckets; i++) {
            bucketSizes[i] = source.getShort();
        }
        return bucketSizes;
    }

    public int getNumBuckets() {
        return numBuckets;
    }

    public ByteBuffer getBucket(int index) {
        return buckets[index - 1].duplicate();
    }
}
