package org.rimasu.gkern.grid.api;


public interface Table {

    int getNumColumns();

    int getNumRows();

    RowCursor createCursor();

    /**
     * Get a range of values that includes all the values in the table. Returned result may
     * be bigger than range of table, but must not be smaller.
     * @return range of values.
     */
    Range getValueRange();

    /**
     * Shorten this table so that it now has the specified
     * number of rows.
     * @param numRows number of rows in the view, must be
     *        less than or equal to getNumRows();
     */
    void truncateTo(int numRows);


}
