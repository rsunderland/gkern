package org.rimasu.gkern.grid.api;


public class KernelException extends RuntimeException {

    private final KernelErrorCode code;

    public KernelException(KernelErrorCode code) {
        super(code.getId());
        this.code = code;
    }

    public KernelErrorCode getCode() {
        return code;
    }
}
