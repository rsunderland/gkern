package org.rimasu.gkern.grid.cursors;

import org.rimasu.gkern.grid.api.ForwardRowCursor;

/**
 * Dual cursor that only shows that values that are in the left cursor that are not in the right cursor.
 * Assumes that the rows in the left and right cursor are fully sorted (no duplicates).
 */
public class SymDifferenceCursor extends DualForwardCursor implements ForwardRowCursor {

    private int ignoredCount;

    public SymDifferenceCursor(ForwardRowCursor left, ForwardRowCursor right) {
        super(left, right);
    }

    @Override
    public boolean moveToNextRow() {
        do {
            step();
            if (onLeft() && !onRight()) {
                return true;
            } else if (onRight() && !onLeft()) {
                ignoredCount++;
            }
        } while (!isFinished());

        return false;
    }

    /**
     * Number of entries in right cursor that did not match left cursor. If ignored count
     * is zero then right cursor was a subset of left cursor.
     * @return number of right entries that had no effect.
     */
    public int getIgnoredCount() {
        return ignoredCount;
    }

}
