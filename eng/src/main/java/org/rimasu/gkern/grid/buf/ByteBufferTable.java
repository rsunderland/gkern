package org.rimasu.gkern.grid.buf;

import org.rimasu.gkern.grid.api.*;
import org.rimasu.gkern.grid.cursors.ArrayRowCursor;

import java.nio.ByteBuffer;

class ByteBufferTable extends BaseTable {

    private final ByteBuffer data;

    ByteBufferTable(int numColumns, int numRows, ByteBuffer buffer) {
        super(numColumns, numRows);
        this.data = buffer;
    }

    @Override
    public RowCursor createCursor() {
        return new ByteBufferArrayRowCursor(getNumRows(), numColumns);
    }


    private class ByteBufferArrayRowCursor extends ArrayRowCursor {

        private ByteBufferArrayRowCursor(int numRows, int numColumns) {
            super(numRows, numColumns);
            moveToRow(0);
        }

        protected void setOffsetValue(int offset, int intValue) {
            checkShort(intValue);
            data.put(offset, (byte)intValue);
        }

        private void checkShort(int intValue) {
            if(intValue > Byte.MAX_VALUE) {
                throw new KernelException(KernelErrorCode.VALUE_OVERFLOW);
            }
            if(intValue <= Byte.MIN_VALUE) {
                throw new KernelException(KernelErrorCode.VALUE_UNDERFLOW);
            }
        }

        protected int getOffsetValue(int offset) {
            return data.get(offset);
        }

        protected void clearOffsetValue(int offset) {
            data.put(offset, Byte.MIN_VALUE);
        }

        protected boolean isOffsetClear(int offset) {
            return data.get(offset) == Byte.MIN_VALUE;
        }
    }

}
